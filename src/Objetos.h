#ifndef __Objetos__
#define __Objetos__

#include "Parametros.h"
#include "Vetor2D.h"
#include "CamadaTile.h"
#include <memory>

class Objetos{ //Classe abstrata Objetos
public:
	//Declarando m�todos virtuais puros (virtual = 0) para definir abstra��o
	virtual void Carregar(std::unique_ptr<Parametros> const &P) = 0; //Usando unique_ptr para assegurar que o ponteiro seja deletado ao sair do escopo
	virtual void Desenhar() = 0;
	virtual void Atualizar() = 0;
	virtual void Limpar() = 0;
	virtual void Colisao() = 0;
	Vetor2D ObterPosicao() { return m_Posicao; }
	Vetor2D ObterVelocidade() { return m_Velocidade; }
	Vetor2D ObterAceleracao() { return m_Aceleracao; }
	void SetarPosicao(Vetor2D Posicao) { m_Posicao = Posicao; }
	int ObterW() { return m_W; }
	int ObterH() { return m_H; }
	int ObterAlpha() { return m_Alpha; }
	bool Atualizando(){ return m_bAtualizando; }
	void SetarAtualizando(bool atualizando){ m_bAtualizando = atualizando; }
	void SetarAlpha(int Alpha){ m_Alpha = Alpha; }
	void SetarColisaoCamadas(std::vector<CamadaTile*> Camadas){ m_ColisaoCamadas = Camadas; }
	std::string ObterTexturaID(){ return m_TexturaID; }

protected:
	Objetos() : m_Posicao(0, 0), m_Aceleracao(0, 0), m_Velocidade(0, 0), m_FatCurva(2.17654987321), m_MaxVeloc(3.50), m_W(0), m_H(0), m_Frame(0), m_AnimSpeed(0), m_NumFrames(0), m_Coluna(0), m_Angulo(0), m_Alpha(255), m_bAtualizando(false) {}
	virtual ~Objetos() {}

	//Declarando membros comuns a todos as classes derivadas dessa
	Vetor2D m_Posicao;
	Vetor2D m_Velocidade;
	Vetor2D m_Aceleracao;

	float m_FatCurva;
	float m_MaxVeloc;
	int m_W;
	int m_H;
	int m_Frame;
	int m_AnimSpeed;
	int m_NumFrames;
	int m_Coluna;
	double m_Angulo;
	int m_Alpha;
	SDL_Point * m_pPonto;
	SDL_RendererFlip m_Flip;
	bool m_bAtualizando;

	std::vector<CamadaTile*> m_ColisaoCamadas;

	std::string m_TexturaID;
};

#endif