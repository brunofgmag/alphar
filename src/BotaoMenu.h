#ifndef __BotaoMenu__
#define __BotaoMenu__

#include "Parametros.h"
#include "ManipuladorObjetos.h"
#include "Carros.h"

class BotaoMenu : public Carros{
public:
	BotaoMenu();

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();

	void SetarCallback(void(*callback)()) { m_Callback = callback; }
	int ObterCallbackID() { return m_CallbackID; }

private:
	enum EstadoBotao{
		MOUSE_FORA = 0,
		MOUSE_DENTRO = 1,
		CLICK = 2
	};

	void (*m_Callback)(); //m_Callback � um ponteiro de uma fun��o que n�o recebe argumentos e retorna void
	bool m_bSolto;
	int m_CallbackID;
};

class CriadorBotaoMenu : public CriadorBase{
	Carros * CriarObjeto() const{
		return new BotaoMenu();
	}
};

#endif