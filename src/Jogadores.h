#ifndef __Jogadores__
#define __Jogadores__

#ifndef PI
#define PI 3.141592653589793238462643383279502884
#endif

#include "Carros.h"
#include <ostream>

class Jogadores : public Carros{
public:
	~Jogadores() {}

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();
	void Colisao();
	void Checkpoints(std::string ID);
	int ObterCheckpoint(){ return m_Checkpoint; }
	int ObterVolta(){ return m_NumVoltas; }
	void SetarRodando(bool Rodando){ m_bRodando = Rodando; }
	bool ObterRodando(){ return m_bRodando; }
	std::string ObterNome(){ return m_Nome; }
	std::string Tipo() { return "Jogadores"; }

protected:
	Jogadores();

	bool m_bIniciado = false;
	bool m_bMovendo = false;
	bool m_bRodando = false;
	float m_EscalaY;
	float m_EscalaX;
	float m_TempSpeed = 3.15;
	float m_Modulo;
	int m_Checkpoint = 0;
	int m_LastCheck = 7;
	int m_NextCheck = 0;
	int m_CheckID = 0;
	int m_NumVoltas = 0;
	int m_TipoCarro;
	std::string m_Nome;
};


#endif