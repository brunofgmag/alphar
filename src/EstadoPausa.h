#ifndef __EstadoPausa__
#define __EstadoPausa__

#include "Objetos.h"
#include "EstadosMenu.h"
#include <vector>

class EstadoPausa : public EstadosMenu{ //Classe que define o estado de jogo Menu
public:
	void Atualizar();
	void Desenhar();

	bool AoEntrar();
	bool AoSair();

	std::string ObterID() const { return s_PausaID; }

private:
	virtual void SetarCallbacks(const std::vector<Callback> &callbacks);

	static const std::string s_PausaID;
	std::vector<Objetos*> m_pObjeto;

	//Fun��es de callback para os itens do menu
	static void s_PausaParaMenu();
	static void s_PausaParaJogo();
};

#endif