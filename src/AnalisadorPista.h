#ifndef __AnalisadorPista__
#define __AnalisadorPista__

#include "tinyxml.h"
#include "Camadas.h"
#include "SDL.h"
#include "zlib.h"
#include "base64.h"
#include <string>
#include <vector>

class Pista;
struct Tileset;
class CamadaTile;
class Objeto;

class AnalisadorPista{
public:
	Pista * AnalisarPista(const char *ArquivoPista);

private:
	void AnalisarTilesets(TiXmlElement *pRaizTileset, std::vector<Tileset> *pTilesets);
	void AnalisarTileCamada(TiXmlElement *pTileElement, std::vector<Camadas*> *pCamadas, const std::vector<Tileset> *pTilesets, std::vector<CamadaTile*> *pCamadasColisao);
	void AnalisarTexturas(TiXmlElement *pTexturaRaiz);
	void AnalisarCamadaObjeto(TiXmlElement *pElementoObjeto, std::vector<Camadas*> *pCamadas, Pista *pPista);
	void AnalisarObjetosColisiveis(TiXmlElement *pElementoObjeto, std::vector<Objeto*> *pObjetosColisiveis, Pista *pPista);
	void AnalisarCheckpoints(TiXmlElement *pElementoObjeto, std::vector<Objeto*> *pCheckpoints, Pista *pPista);

	int m_TileTamanho;
	int m_W;
	int m_H;

	bool m_bColisivel = false;

	SDL_RendererFlip m_Flip;
};

#endif