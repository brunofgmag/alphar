#include "EstadoJogo.h"
#include "alphaR.h"

const std::string EstadoJogo::s_JogoID = "JOGO";

void EstadoJogo::CalcularHora(){
	if (m_bPausado == true){ //Se o jogo foi pausado
		m_TickRetorno = SDL_GetTicks() - m_TickInicial; //Salva o Tick de retorno
		m_TickReal = (SDL_GetTicks() - m_TickInicial) - (m_TickRetorno - m_TickPausa); //Calcula o Tick real
		m_bPausado = false; //Jogo n�o est� mais pausado!
	}
	else{
		m_TickReal = (SDL_GetTicks() - m_TickInicial) - (m_TickRetorno - m_TickPausa); //Calcula o Tick real
	}

	m_Segundo = m_TickReal - 5500; //Salva o tick nos segundos
	m_Milisegundos = m_Segundo % 1000;
	m_Segundo /= 1000;
	m_Minuto = m_Segundo / 60;
	m_Segundo = m_Segundo % 60;
	m_Segundo = m_Segundo;

	if (m_TempoBuffer != (m_Milisegundos / 100)){ //Buffer de tempo para que a atualiza��o seja feita a cada mudan�a de milisegundo (GRANDE SALVADOR DE RECURSOS!!!)
		//Zera o valor da StringStream para evitar que o valor atual seja adicionado ao anterior
		StringStream.str(std::string());

		if (m_Segundo <= 0 && m_Milisegundos < 0){
			if (m_Segundo > -10){
				StringStream << "-00:0" << std::to_string(abs(m_Segundo)) << ":" << std::to_string(abs(m_Milisegundos / 100));
			}
			else{
				StringStream << "-00:" << std::to_string(abs(m_Segundo)) << ":" << std::to_string(abs(m_Milisegundos / 100));
			}
			
		}
		else {
			if (m_Minuto < 10){
				m_Tempo[1] = std::to_string(m_Minuto);
				StringStream << "0" << std::to_string(m_Minuto) << ":";
			}
			else{
				StringStream << std::to_string(m_Minuto) << ":";
			}

			if (m_Segundo < 10){
				StringStream << "0" << std::to_string(m_Segundo) << ":";
			}
			else{
				StringStream << std::to_string(m_Segundo) << ":";
			}

			StringStream << std::to_string(m_Milisegundos / 100);
		}
		
		m_S = StringStream.str();
		StringStream.clear();
		m_pTempo = m_S.c_str();
	}

	//Para debug!!
	//SDL_Log("Real: %d - Pausa: %d - Retorno: %d - SDL: %d - Buffer: %d - Mili: %d", m_TickReal, m_TickPausa, m_TickRetorno, SDL_GetTicks(), m_TempoBuffer, m_Milisegundos / 100);
}

void EstadoJogo::Atualizar(){
	if (!m_bGameOver){
		this->CalcularHora();
	}

	if (pPista != 0){
		pPista->Atualizar();
		if (m_VoltaP1Buffer != pPista->ObterJogadorUm()->ObterVolta()){
			m_VoltasJogadorUm = std::to_string(pPista->ObterJogadorUm()->ObterVolta());
			if (pPista->ObterJogadorUm()->ObterVolta() == 3){
				pPista->ObterJogadorUm()->SetarRodando(false);
				pPista->ObterJogadorDois()->SetarRodando(false);
				m_bGameOver = true;
				if (pPista->ObterJogadorDois()->ObterVolta() != 3)
					m_Vencedor = 1;
			}
		}

		if (m_VoltaP2Buffer != pPista->ObterJogadorDois()->ObterVolta()){
			m_VoltasJogadorDois = std::to_string(pPista->ObterJogadorDois()->ObterVolta());
			if (pPista->ObterJogadorDois()->ObterVolta() == 3){
				pPista->ObterJogadorUm()->SetarRodando(false);
				pPista->ObterJogadorDois()->SetarRodando(false);
				m_bGameOver = true;
				if (pPista->ObterJogadorUm()->ObterVolta() != 3)
					m_Vencedor = 2;
			}
		}

		if (m_pJogadorUm == nullptr){
			if (dynamic_cast<JogadorUm*>(pPista->ObterJogadorUm())){ //Verifica se o jogador � do tipo JogadoUm
				m_pJogadorUm = pPista->ObterJogadorUm();
				m_JogadorUmNome = m_pJogadorUm->ObterNome();
			}
		}

		if (m_pJogadorDois == nullptr){
			if (dynamic_cast<JogadorDois*>(pPista->ObterJogadorDois())){ //Verifica se o jogador � do tipo JogadoDois
				m_pJogadorDois = pPista->ObterJogadorDois();
				m_JogadorDoisNome = m_pJogadorDois->ObterNome();
				m_TickInicial = SDL_GetTicks(); //Come�a a contar somente ap�s a sele��o do nome do jogador dois
			}
		}
		if (!m_bGameOver){
			if (m_Milisegundos / 100 >= 0 && m_Segundo >= 0){
				m_pJogadorUm->SetarRodando(true);
				m_pJogadorDois->SetarRodando(true);
			}
			else{
				m_pJogadorUm->SetarRodando(false);
				m_pJogadorDois->SetarRodando(false);
			}
		}
	}
	if (!m_bGameOver){
		if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_ESCAPE) && (SDL_GetTicks() - m_TickRetorno) > 300){ //Usar isso no EstadoPausa
			m_TickPausa = m_TickReal; //Salva o Tick de pausa com o �ltimo Tick real
			m_bPausado = true; //Jogo foi pausado
			alphaR::Instancia()->ObterEstado()->ChamarEstado(new EstadoPausa());
		}
	}
	else{
		if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_ESCAPE) && (SDL_GetTicks() - m_TickRetorno) > 300){ //Usar isso no EstadoPausa
			alphaR::Instancia()->ObterEstado()->MudarEstado(new EstadoMenu());
		}
	}

	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Atualizar();

		if (dynamic_cast<TextosDinamicos*>(m_pObjeto[i])){ //Verifica se a instancia do m_pObjeto[i] � do tipo TextosDinamicos
			//S� efetua o dynamic_cast para salvar a convers�o do m_pObjeto[i] se houver altera��o nos dados (poupa aprox. 4,2% de CPU em um Core i7 2600)
			if (m_TempoBuffer != (m_Milisegundos / 100) || m_VoltaP1Buffer != std::atoi(m_VoltasJogadorUm.c_str()) || m_VoltaP2Buffer != std::atoi(m_VoltasJogadorDois.c_str()) || (m_Vencedor != 0 && m_bGameOver)){
				m_pTextosDinamicos = dynamic_cast<TextosDinamicos*>(m_pObjeto[i]); //Converte a instancia do m_pObjeto[i] para TextosDinamicos e armazena a convers�o no m_pTextosDinamicos
			}

			//Executa a altera��o para cada texto din�mico, um de cada vez, se houverem altera��o nos mesmos.
			if (m_pTextosDinamicos->ObterTexturaID() == std::string("D_tempo") && m_TempoBuffer != (m_Milisegundos / 100)){
				m_pTextosDinamicos->SetarTexto("D_tempo", m_pTempo); //Atualiza o texto

				if (m_TempoBuffer == 9) //Reseta o buffer quando o mesmo chegar a 9
					m_TempoBuffer = -1;

				m_TempoBuffer++; //Coloca o buffer um passo a frente (e um passo atr�s dos milisegundos)
			}
			else if (m_pTextosDinamicos->ObterTexturaID() == std::string("D_jogadorumvolta") && m_VoltaP1Buffer != std::atoi(m_VoltasJogadorUm.c_str())){
				m_pTextosDinamicos->SetarTexto("D_jogadorumvolta", m_VoltasJogadorUm); //Atualiza a volta do Jogador 1
				m_VoltaP1Buffer++;
			}
			else if (m_pTextosDinamicos->ObterTexturaID() == std::string("D_jogadordoisvolta") && m_VoltaP2Buffer != std::atoi(m_VoltasJogadorDois.c_str())){
				m_pTextosDinamicos->SetarTexto("D_jogadordoisvolta", m_VoltasJogadorDois); //Atualiza a volta do Jogador 2
				m_VoltaP2Buffer++;
			}
			else if (m_pTextosDinamicos->ObterTexturaID() == std::string("D_jogadorumnome") && m_JogadorUmNome != std::string()){
				m_pTextosDinamicos->SetarTexto("D_jogadorumnome", m_JogadorUmNome); //Atualiza o texto
			}
			else if (m_pTextosDinamicos->ObterTexturaID() == std::string("D_jogadordoisnome") && m_JogadorDoisNome != std::string()){
				m_pTextosDinamicos->SetarTexto("D_jogadordoisnome", m_JogadorDoisNome); //Atualiza o texto
			}
			else if (m_pTextosDinamicos->ObterTexturaID() == std::string("D_jogadorvencedor") && m_Vencedor != 0 && m_bGameOver){
				if (m_Vencedor == 1){
					m_pTextosDinamicos->SetarTexto("D_jogadorvencedor", m_Vencedor1);
				}
				else if (m_Vencedor == 2){
					m_pTextosDinamicos->SetarTexto("D_jogadorvencedor", m_Vencedor2);
				}
			}
		}
	}
}

void EstadoJogo::Desenhar(){
	if (pPista != 0){
		pPista->Desenhar();
	}

	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Desenhar();
	}
}

bool EstadoJogo::AoEntrar(){
	AnalisadorEstados analisadorEstados;
	analisadorEstados.AnalisarEstado("res/Estados.xml", s_JogoID, &m_pObjeto, &m_TexturasID);

	AnalisadorPista analisadorPista;
	pPista = analisadorPista.AnalisarPista("levels/Pista.tmx");

	m_bGameOver = false;

	SDL_Log("Entrando no estado JOGO");
	return true;
}

bool EstadoJogo::AoSair(){
	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Limpar();
	}
	pPista->Limpar();
	m_pObjeto.clear();
	SDL_Log("Saindo do estado JOGO");
	return true;
}