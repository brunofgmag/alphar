#include "Pista.h"

void Pista::Desenhar(){
	for (int i = 0; i < m_Camadas.size(); i++){
		m_Camadas[i]->Desenhar();
	}
}

void Pista::Atualizar(){
	for (int i = 0; i < m_Camadas.size(); i++){
		m_Camadas[i]->Atualizar(this);
	}
}

void Pista::Limpar(){
	for (int i = 0; i < m_Camadas.size(); i++){
		m_Camadas[i]->Limpar();
	}
}