#ifndef __ManipuladorEstados__
#define __ManipuladorEstados__

#include "EstadosJogo.h"
#include <vector>

class ManipuladorEstados{ //Manipula a troca de estados
public:
	void ChamarEstado(EstadosJogo * pEstado); //Chama um estado sem remover o anterior
	void MudarEstado(EstadosJogo * pEstado); //Muda o estado removendo o anterior
	void RemoverEstado(); //Remove o estado atual
	void Atualizar();
	void Desenhar();
	void Limpar();

private:
	std::vector<EstadosJogo*> m_EstadosJogo;
};

#endif