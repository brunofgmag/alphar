#ifndef __EstadoMenu__
#define __EstadoMenu__

#include "Objetos.h"
#include "EstadosMenu.h"
#include <vector>

class EstadoMenu : public EstadosMenu{ //Classe que define o estado de jogo Menu
public:
	void Atualizar();
	void Desenhar();

	bool AoEntrar();
	bool AoSair();

	std::string ObterID() const { return s_MenuID; }

private:
	virtual void SetarCallbacks(const std::vector<Callback> &callbacks);

	static const std::string s_MenuID;
	std::vector<Objetos*> m_pObjeto;

	//Fun��es de callback para os itens do menu
	static void s_MenuParaJogo();
	static void s_SairDoMenu();

	int m_Iniciar = SDL_GetTicks();
	int m_Alpha = 0;

	bool m_bAlpha = false;
	bool m_bIniciado = false;
};

#endif