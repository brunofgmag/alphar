#ifndef __CarroAzul__
#define __CarroAzul__

#include "Objeto.h"
#include "ManipuladorObjetos.h"


class CarroAzul : public Objeto{
public:
	CarroAzul();
	~CarroAzul() {}

	void Carregar(const Parametros * P);
	void Desenhar();
	void Atualizar();
	void Limpar();

private:
	void EntradaDados();
};

class CriadorCarroAzul : public CriadorBase{
	Objeto * CriarObjeto() const{
		return new CarroAzul();
	}
};

#endif