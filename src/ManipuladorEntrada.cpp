#include "ManipuladorEntrada.h"
#include "alphaR.h"

ManipuladorEntrada * ManipuladorEntrada::s_pInstancia = 0; //Zera o contador de instancias

ManipuladorEntrada::ManipuladorEntrada() : m_PosicaoMouse(0, 0), m_EstadoTecla(0) {
	for (int i = 0; i < 3; i++){
		m_EstadoBotao.push_back(false); //Salva os 3 bot�es do mouse em um vetor com valor padr�o false (N�o pressionados)
	}
}

ManipuladorEntrada::~ManipuladorEntrada(){
	//Deleta tudo que for alocado dinamicamente
	delete m_EstadoTecla;

	//Limpa os vetores
	m_EstadoBotao.clear();
}

void ManipuladorEntrada::Atualizar(){
	SDL_Event Evento; //Cria objeto de eventos

	while (SDL_PollEvent(&Evento)){ //Enquanto acontecer um evento
		switch (Evento.type){
		case SDL_QUIT:
			alphaR::Instancia()->Clean();
			break;
		case SDL_MOUSEMOTION:
			MouseMovimento(Evento);
			break;
		case SDL_MOUSEBUTTONDOWN:
			MouseBotaoPressionado(Evento);
			break;
		case SDL_MOUSEBUTTONUP:
			MouseBotaoSolto(Evento);
		case SDL_KEYDOWN:
			OnTeclaPressionada();
			break;
		case SDL_KEYUP:
			OnTeclaSolta();
			break;
		default:
			break;
		}
	}
}

void ManipuladorEntrada::MouseBotaoPressionado(SDL_Event &Evento){
	if (Evento.type == SDL_MOUSEBUTTONDOWN){ //Detecta se algum bot�o do mouse foi pressionado
		if (Evento.button.button == SDL_BUTTON_LEFT){
			m_EstadoBotao[LEFT] = true;
		}
		if (Evento.button.button == SDL_BUTTON_MIDDLE){
			m_EstadoBotao[MIDDLE] = true;
		}
		if (Evento.button.button == SDL_BUTTON_RIGHT){
			m_EstadoBotao[RIGHT] = true;
		}
	}
}

void ManipuladorEntrada::MouseBotaoSolto(SDL_Event &Evento){
	if (Evento.type == SDL_MOUSEBUTTONUP){ //Detecta se algum bot�o do mouse foi solto
		if (Evento.button.button == SDL_BUTTON_LEFT){
			m_EstadoBotao[LEFT] = false;
		}
		if (Evento.button.button == SDL_BUTTON_MIDDLE){
			m_EstadoBotao[MIDDLE] = false;
		}
		if (Evento.button.button == SDL_BUTTON_RIGHT){
			m_EstadoBotao[RIGHT] = false;
		}
	}
}

void ManipuladorEntrada::Resetar(){
	m_EstadoBotao[LEFT] = false;
	m_EstadoBotao[MIDDLE] = false;
	m_EstadoBotao[RIGHT] = false;
}

void ManipuladorEntrada::MouseMovimento(SDL_Event &Evento){
	if (Evento.type == SDL_MOUSEMOTION){
		m_PosicaoMouse.SetarX(Evento.motion.x);
		m_PosicaoMouse.SetarY(Evento.motion.y);
	}
}

void ManipuladorEntrada::OnTeclaPressionada(){
	m_EstadoTecla = SDL_GetKeyboardState(0);
}

void ManipuladorEntrada::OnTeclaSolta(){
	m_EstadoTecla = SDL_GetKeyboardState(0);
}

bool ManipuladorEntrada::TeclaPressionada(SDL_Scancode Tecla) const{
	if (m_EstadoTecla != 0){
		if (m_EstadoTecla[Tecla] == 1)
			return true;
		else
			return false;
	}

	return false;
}