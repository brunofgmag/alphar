#include "AnalisadorPista.h"
#include "Pista.h"
#include "alphaR.h"
#include "CamadaTile.h"
#include "CamadaObjeto.h"
#include "JogadorUm.h"
#include "JogadorDois.h"
#include "Objeto.h"


Pista *AnalisadorPista::AnalisarPista(const char *ArquivoPista){
	//Cria o arquivo XML
	TiXmlDocument PistaDocumento;

	//Carrega o arquivo de pista
	if (!PistaDocumento.LoadFile(ArquivoPista)){
		SDL_Log("%s \n", PistaDocumento.ErrorDesc());
		return false;
	}

	//Cria o objeto Pista
	Pista *pPista = new Pista();

	//Obter Elemento raiz
	TiXmlElement *pRaiz = PistaDocumento.RootElement();

	pRaiz->Attribute("tilewidth", &m_TileTamanho);
	pRaiz->Attribute("width", &m_W);
	pRaiz->Attribute("height", &m_H);

	//Propriedades
	TiXmlElement *pProperties = pRaiz->FirstChildElement();

	//Analisa texturas necess�rias das propriedades do mapa
	for (TiXmlElement *e = pProperties->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("property")){
			AnalisarTexturas(e);
		}
	}

	//Analisa os tilesets
	for (TiXmlElement *e = pRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("tileset")){
			AnalisarTilesets(e, pPista->ObterTilesets());
		}
	}

	//Analisa qualquer camada objeto
	for (TiXmlElement *e = pRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("layer")){
			if (e->FirstChildElement()->Value() == std::string("data") || (e->FirstChildElement()->NextSiblingElement() != 0 && e->FirstChildElement()->NextSiblingElement()->Value() == std::string("data"))){
				AnalisarTileCamada(e, pPista->ObterCamadas(), pPista->ObterTilesets(), pPista->ObterCamadasColisao());
			}
		}
		if (e->Value() == std::string("objectgroup")){
			if (e->FirstChildElement()->Value() == std::string("object") && e->Attribute("name") != std::string("Collidable") && e->Attribute("name") != std::string("Checkpoint")){
				AnalisarCamadaObjeto(e, pPista->ObterCamadas(), pPista);
			}
			else if (e->FirstChildElement()->Value() == std::string("object") && e->Attribute("name") == std::string("Collidable")){
				AnalisarObjetosColisiveis(e, pPista->ObterObjetosColisao(), pPista);
			}
			else if (e->FirstChildElement()->Value() == std::string("object") && e->Attribute("name") == std::string("Checkpoint")){
				AnalisarCheckpoints(e, pPista->ObterObjetosCheckpoint(), pPista);
			}
		}
	}
	//pPista->ObterJogador()->SetarColisaoCamadas(pPista->ObterCamadaColisivel());

	return pPista;
}

void AnalisadorPista::AnalisarTexturas(TiXmlElement *pTexturaRaiz){
	std::string Tag = "imgs/";
	std::string Valor = Tag.append(pTexturaRaiz->FirstChildElement()->Attribute("value"));
	std::string Nome = pTexturaRaiz->Attribute("name");
	Textura::Instancia()->Carregar(Valor, Nome, alphaR::Instancia()->GetRenderer());
}

void AnalisadorPista::AnalisarCheckpoints(TiXmlElement *pElementoObjeto, std::vector<Objeto*> *pCheckpoints, Pista *pPista){
	for (TiXmlElement *e = pElementoObjeto->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("object")){
			int x = 0, y = 0, h = 0, w = 0, id = 0;

			e->Attribute("x", &x);
			e->Attribute("y", &y);
			e->Attribute("height", &h);
			e->Attribute("width", &w);
			//Obt�m os valores "property"
			for (TiXmlElement * properties = e->FirstChildElement(); properties != NULL; properties = properties->NextSiblingElement()){
				if (properties->Value() == std::string("properties")){
					for (TiXmlElement * pproperty = properties->FirstChildElement(); pproperty != NULL; pproperty = pproperty->NextSiblingElement()){
						if (pproperty->Value() == std::string("property")){
							//Carregando os atributos nas vari�veis
							if (pproperty->Attribute("name") == std::string("id")){
								pproperty->Attribute("value", &id);
							}
						}
					}
				}
			}

			//Cria uma instancia de uma classe abstrata de objetos
			Objeto *pObjetos = new Objeto();
			
			pObjetos->Carregar(std::unique_ptr<Parametros>(new Parametros(std::to_string(id), x, y, h, w)));

			pCheckpoints->push_back(pObjetos);
		}
	}
}

void AnalisadorPista::AnalisarObjetosColisiveis(TiXmlElement *pElementoObjeto, std::vector<Objeto*> *pObjetosColisiveis, Pista *pPista){
	for (TiXmlElement *e = pElementoObjeto->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("object")){
			//Cria uma instancia de uma classe abstrata de objetos
			Objeto *pObjetos = new Objeto();

			int x = 0, y = 0, h = 0, w = 0;
			//std::string texturaID = "";

			//texturaID = e->Attribute("texturaID");
			e->Attribute("x", &x);
			e->Attribute("y", &y);
			e->Attribute("height", &h);
			e->Attribute("width", &w);

			pObjetos->Carregar(std::unique_ptr<Parametros>(new Parametros("", x, y, h, w)));

			pObjetosColisiveis->push_back(pObjetos);
		}
	}
}

void AnalisadorPista::AnalisarCamadaObjeto(TiXmlElement * pElementoObjeto, std::vector<Camadas*> *pCamadas, Pista *pPista){
	//Cria uma camada objeto
	CamadaObjeto* pCamadaObjeto = new CamadaObjeto();

	for (TiXmlElement *e = pElementoObjeto->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("object")){
			int x, y, h, w, frame = 0, numFrames = 0, animSpeed = 0, coluna = 0, callbackID = 0, flip = 0;
			double angulo = 0.0;
			std::string TexturaID, Tipo;

			//Obtem os valores iniciais para X e Y
			e->Attribute("x", &x);
			e->Attribute("y", &y);
			Tipo = e->Attribute("type");
			Objetos * pObjeto = ManipuladorObjetos::Instancia()->Criar(Tipo);

			//Obt�m os valores "property"
			for (TiXmlElement * properties = e->FirstChildElement(); properties != NULL; properties = properties->NextSiblingElement()){
				if (properties->Value() == std::string("properties")){
					for (TiXmlElement * pproperty = properties->FirstChildElement(); pproperty != NULL; pproperty = pproperty->NextSiblingElement()){
						if (pproperty->Value() == std::string("property")){
							//Carregando os atributos nas vari�veis
							if (pproperty->Attribute("name") == std::string("numFrames")){
								pproperty->Attribute("value", &numFrames);
							}else if(pproperty->Attribute("name") == std::string("h")){
								pproperty->Attribute("value", &h);
							}else if(pproperty->Attribute("name") == std::string("texturaID")){
								TexturaID = pproperty->Attribute("value");
							}else if(pproperty->Attribute("name") == std::string("w")){
								pproperty->Attribute("value", &w);
							}else if(pproperty->Attribute("name") == std::string("callbackID")){
								pproperty->Attribute("value", &callbackID);
							}else if(pproperty->Attribute("name") == std::string("animSpeed")){
								pproperty->Attribute("value", &animSpeed);
							}else if(pproperty->Attribute("name") == std::string("frame")){
								pproperty->Attribute("value", &frame);
							}else if(pproperty->Attribute("name") == std::string("angulo")){
								pproperty->Attribute("value", &angulo);
							}else if(pproperty->Attribute("name") == std::string("flip")){
								pproperty->Attribute("value", &flip);
							}else if(pproperty->Attribute("name") == std::string("coluna")){
								pproperty->Attribute("value", &coluna);
							}
						}
					}
				}
			}

			switch (flip){
			case 1:
				m_Flip = SDL_FLIP_HORIZONTAL;
				break;
			case 2:
				m_Flip = SDL_FLIP_VERTICAL;
				break;
			default:
				m_Flip = SDL_FLIP_NONE;
			}
			if (Tipo == "JogadorUm"){
				//JogadorUm *pJogadorUm = nullptr;
				//pJogadorUm = dynamic_cast<JogadorUm*>(pObjeto);
				//pJogadorUm->Carregar(std::unique_ptr<Parametros>(new Parametros(TexturaID, x, y, h, w, frame, numFrames, animSpeed, coluna, angulo, 0, m_Flip, callbackID)));
				pPista->SetarJogadorUm(dynamic_cast<JogadorUm*>(pObjeto));
				//pCamadaObjeto->ObterObjetos()->push_back(pJogadorUm);
			}
			else if (Tipo == "JogadorDois"){
				pPista->SetarJogadorDois(dynamic_cast<JogadorDois*>(pObjeto));
			}
			//else{
				#pragma warning(suppress: 6001)
				pObjeto->Carregar(std::unique_ptr<Parametros>(new Parametros(TexturaID, x, y, h, w, frame, numFrames, animSpeed, coluna, angulo, 0, m_Flip, callbackID)));
				pObjeto->SetarColisaoCamadas(pPista->ObterCamadaColisivel());
				pCamadaObjeto->ObterObjetos()->push_back(pObjeto);
			//}
		}
	}

	
	pCamadas->push_back(pCamadaObjeto);
}

void AnalisadorPista::AnalisarTilesets(TiXmlElement *pRaizTileset, std::vector<Tileset> *pTilesets){
	//Adiciona o tileset ao gerenciador de texturas
	std::string Tag = "assets/";
	std::string Source = Tag.append(pRaizTileset->FirstChildElement()->Attribute("source"));
	std::string Nome = pRaizTileset->Attribute("name");
	Textura::Instancia()->Carregar(Source, Nome, alphaR::Instancia()->GetRenderer());

	//Cria um objeto tileset
	Tileset tileset;
	pRaizTileset->FirstChildElement()->Attribute("width", &tileset.W);
	pRaizTileset->FirstChildElement()->Attribute("height", &tileset.H);
	pRaizTileset->Attribute("firstgid", &tileset.PrimeiroGridID);
	pRaizTileset->Attribute("tilewidth", &tileset.TileW);
	pRaizTileset->Attribute("tileheight", &tileset.TileH);
	//pRaizTileset->Attribute("spacing", &tileset.Espacamento);
	//pRaizTileset->Attribute("margin", &tileset.Margem);
	tileset.Espacamento = 0;
	tileset.Margem = 0;
	tileset.Nome = pRaizTileset->Attribute("name");
	tileset.NumColunas = tileset.W / (tileset.TileW + tileset.Espacamento);

	/*
	std::cout << tileset.W << std::endl;
	std::cout << tileset.H << std::endl;
	std::cout << tileset.TileW << std::endl;
	std::cout << tileset.TileH << std::endl;
	std::cout << tileset.Espacamento << std::endl;
	std::cout << tileset.Margem << std::endl;
	std::cout << tileset.Nome << std::endl;
	std::cout << tileset.NumColunas << std::endl;
	std::cout << tileset.PrimeiroGridID << std::endl;
	*/

	pTilesets->push_back(tileset);
}

void AnalisadorPista::AnalisarTileCamada(TiXmlElement *pTileElement, std::vector<Camadas*> *pCamadas, const std::vector<Tileset> *pTilesets, std::vector<CamadaTile*> *pCamadasColisao){
	//Cria um objeto CamadaTile
	CamadaTile * pCamadaTile = new CamadaTile(m_TileTamanho, *pTilesets);

	//Tile data
	std::vector<std::vector<int>> data;

	std::string IDsDecodificados;
	TiXmlElement *pDataNode = nullptr;

	for (TiXmlElement* e = pTileElement->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("properties")){
			for (TiXmlElement* pproperty = e->FirstChildElement(); pproperty != NULL; pproperty = pproperty->NextSiblingElement()){
				if (pproperty->Value() == std::string("property")){
					if (pproperty->Attribute("name") == std::string("collidable")){
						m_bColisivel = true;
					}
				}
			}
		}

		if (e->Value() == std::string("data")){
			pDataNode = e;
		}
	}

	for (TiXmlNode *e = pDataNode->FirstChild(); e != NULL; e = e->NextSibling()){
		TiXmlText *Texto = e->ToText();
		std::string t = Texto->Value();
		IDsDecodificados = base64_decode(t);
	}

	//Descomprimir a compressao zlib
	uLongf sizeofids = m_W * m_H * sizeof(int);
	std::vector<int> gids(m_W * m_H);
	uncompress((Bytef*)&gids[0], &sizeofids, (const Bytef*)IDsDecodificados.c_str(), IDsDecodificados.size());

	std::vector<int> LinhaCamada(m_W);

	for (int j = 0; j < m_H; j++){
		data.push_back(LinhaCamada);
	}

	for (int Linhas = 0; Linhas < m_H; Linhas++){
		for (int Colunas = 0; Colunas < m_W; Colunas++){
			data[Linhas][Colunas] = gids[Linhas * m_W + Colunas];
			//std::cout << Linhas << std::endl;
		}
	}

	pCamadaTile->SetarTileIDs(data);

	//Adiciona colisao se necessario
	if (m_bColisivel)
	{
		pCamadasColisao->push_back(pCamadaTile);
	}

	pCamadas->push_back(pCamadaTile);
}