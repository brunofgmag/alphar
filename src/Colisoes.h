#ifndef __Colisoes__
#define __Colisoes__

#include "SDL.h"

class Colisoes{
public:
	static bool Colisao(SDL_Rect *A, SDL_Rect *B){

		int s_Buffer = 4;

		int BufferAH = A->h / s_Buffer;
		int BufferAW = A->w / s_Buffer;
		int BufferBH = B->h / s_Buffer;
		int BufferBW = B->w / s_Buffer;

		//Se o fundo de A � menor que o topo de B - Sem colis�o
		if ((A->y + A->h) - BufferAH <= B->y + BufferBH) { return false; }

		//Se o topo de A � maior que o fundo de B - Sem colis�o
		if (A->y + BufferAH >= (B->y + B->h) - BufferBH) { return false; }

		//Se o lado direito de A for menor do que o lado esquerdo de B - Sem colis�o
		if ((A->x + A->w) - BufferAW <= B->x + BufferBW) { return false; }

		//Se o lado esquerdo de A for maior que o lado direito de B - Sem colis�o
		if (A->x + BufferAW >= (B->x + B->w) - BufferBW) { return false; }

		//Caso contr�rio houve uma colis�o
		return true;
	}

	static bool ColisaoObjetoCarro(SDL_Rect *A, SDL_Rect *B){

		int s_Buffer = 7;

		int BufferAH = A->h / s_Buffer;
		int BufferAW = A->w / s_Buffer;
		int BufferBH = B->h / s_Buffer;
		int BufferBW = B->w / s_Buffer;

		//Se o fundo de A � menor que o topo de B - Sem colis�o
		if ((A->y + A->h) <= B->y) { return false; }

		//Se o topo de A � maior que o fundo de B - Sem colis�o
		if (A->y >= (B->y + B->h)) { return false; }

		//Se o lado direito de A for menor do que o lado esquerdo de B - Sem colis�o
		if ((A->x + A->h/1.3) <= B->x) { return false; }

		//Se o lado esquerdo de A for maior que o lado direito de B - Sem colis�o
		if (A->x>= (B->x + B->w + A->w/2)) { return false; }

		//Caso contr�rio houve uma colis�o
		return true;
	}
};

#endif