#include "Textura.h"
#include "alphaR.h"

Textura * Textura::s_pInstancia = 0; //Zera o contador de instancias

bool Textura::Carregar(std::string Arquivo, std::string ID, SDL_Renderer * Renderer){ //Cria a textura desejada
	SDL_Surface * pTempSuperficie = IMG_Load(Arquivo.c_str()); //Cria a superficie temporaria para imagens

	if (!pTempSuperficie){
		SDL_Log("Nao foi possivel criar a superficie: %s", SDL_GetError());
		return false;
	}/*else
		SDL_Log("Superficie criada com sucesso");*/

	SDL_Texture * Textura = SDL_CreateTextureFromSurface(alphaR::Instancia()->GetRenderer(), pTempSuperficie); //Cria a textura para GPU a partir da superficie

	if (!Textura){
		SDL_Log("Nao foi possivel criar a Textura %s: %s", ID.c_str(), SDL_GetError());
		return false;
	}else{
		SDL_Log("Textura criada com sucesso: %s", ID.c_str());
		m_pTexturas[ID] = Textura;
	}

	SDL_FreeSurface(pTempSuperficie); //Apaga a superficie temporaria

	return true;
}

bool Textura::CarregarTexto(std::string Fonte, std::string ID, SDL_Renderer * Renderer, int Tamanho, std::string Texto, SDL_Color Cor){ //Cria uma textura a partir de um texto
	TTF_Font * pTempFonte = TTF_OpenFont(Fonte.c_str(), Tamanho);

	if (!pTempFonte){
		SDL_Log("Nao foi possivel carregar a fonte: %s", TTF_GetError());
		return false;
	}

	SDL_Surface * pTempSuperficie = TTF_RenderText_Solid(pTempFonte, Texto.c_str(), Cor); //Cria a superficie temporaria

	if (!pTempSuperficie){
		SDL_Log("Nao foi possivel criar a superficie de texto: %s", SDL_GetError());
		return false;
	}/*else
	 SDL_Log("Superficie criada com sucesso");*/

	SDL_Texture * Textura = SDL_CreateTextureFromSurface(alphaR::Instancia()->GetRenderer(), pTempSuperficie); //Cria a textura para GPU a partir da superficie

	if (!Textura){
		SDL_Log("Nao foi possivel criar a Textura %s: %s", ID.c_str(), SDL_GetError());
		return false;
	}
	else{
		//Workaround para n�o mostrar informa��o de textos dinamicos
		if (!(ID.find("D_") != std::string::npos))
			SDL_Log("Textura criada com sucesso: %s", ID.c_str());
		m_pTexturas[ID] = Textura;
	}

	TTF_CloseFont(pTempFonte); //Apaga o arquivo de fonte temporario
	SDL_FreeSurface(pTempSuperficie); //Apaga a superficie temporaria

	return true;
}

void Textura::Desenhar(std::string ID, SDL_Renderer * Renderer, int X, int Y, int H, int W){ //Desenha a textura no render
	//Define posicao e tamanho dos ret�ngulos
	m_Origem.x = 0;
	m_Origem.y = 0;
	m_Destino.h = m_Origem.h = H;
	m_Destino.w = m_Origem.w = W;
	m_Destino.x = X;
	m_Destino.y = Y;

	SDL_RenderCopy(Renderer, m_pTexturas[ID], &m_Origem, &m_Destino);
}

void Textura::DesenharFrame(std::string ID, SDL_Renderer * Renderer, int X, int Y, int H, int W, int Frame, int Coluna, double Angulo, int Alpha, SDL_Point * Ponto, SDL_RendererFlip Flip){ //Desenha a textura animada no render desejado
	//Workaround para sempre escrever texto no tamanho certo
	if (H == 0 && W == 0){
		SDL_QueryTexture(m_pTexturas[ID], NULL, NULL, &W, &H);
	}

	//Define posicao e tamanho dos ret�ngulos
	m_Origem.x = W * Frame;
	m_Origem.y = H * Coluna;
	m_Destino.h = m_Origem.h = H;
	m_Destino.w = m_Origem.w = W;
	m_Destino.x = X;
	m_Destino.y = Y;

	//Seta o alpha da textura
	SDL_SetTextureAlphaMod(m_pTexturas[ID], Alpha);
	SDL_RenderCopyEx(Renderer, m_pTexturas[ID], &m_Origem, &m_Destino, Angulo, Ponto, Flip);
}

void Textura::DesenharTile(std::string ID, int Margem, int Espacamento, int X, int Y, int W, int H, int Linha, int Frame, SDL_Renderer * pRenderer){
	SDL_Rect Origem;
	SDL_Rect Destino;

	Origem.x = Margem + (Espacamento + W) * Frame;
	Origem.y = Margem + (Espacamento + H) * Linha;
	Origem.w = Destino.w = W;
	Origem.h = Destino.h = H;
	Destino.x = X;
	Destino.y = Y;

	//SDL_Log("%d", m_Origem.x);

	SDL_RenderCopyEx(pRenderer, m_pTexturas[ID], &Origem, &Destino, 0, 0, SDL_FLIP_NONE);
}

void Textura::Limpar(){ //Destr�i todas as texturas
	for (std::map<std::string, SDL_Texture*>::iterator it = m_pTexturas.begin(); it != m_pTexturas.end(); it++)
	{
		if (it->second != nullptr){
			SDL_DestroyTexture(it->second);
			SDL_Log("Textura apagada com sucesso: %s", it->first.c_str());
		}
			
	}

	if (!m_pTexturas.empty()){
		SDL_Log("Listagem de texturas limpa!");
		m_pTexturas.clear();
	}
}

void Textura::Apagar(std::string ID){ //Destr�i uma textura espec�fica
	SDL_DestroyTexture(m_pTexturas[ID]);
	m_pTexturas.erase(ID);
	//Workaround para n�o mostrar informa��o de textos dinamicos
	if (!(ID.find("D_") != std::string::npos))
		SDL_Log("Textura apagada com sucesso: %s", ID.c_str());
}