#include "GraficoAnimado.h"
#include "ManipuladorEntrada.h"

GraficoAnimado::GraficoAnimado() : Objeto(){
	m_Frame = 0; //Come�a no frame 0
}

void GraficoAnimado::Carregar(std::unique_ptr<Parametros> const &P){
	Objeto::Carregar(P);
}

void GraficoAnimado::Desenhar(){
	Objeto::Desenhar();
}

void GraficoAnimado::Atualizar(){
	m_Frame = int(((SDL_GetTicks() / (1000 / m_AnimSpeed)) % m_NumFrames));
}

void GraficoAnimado::Limpar(){
	Objeto::Limpar();
}