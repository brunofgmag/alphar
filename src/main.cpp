#include "alphaR.h"

const int FPS = 60; //Quantidade m�xima de frames por segundo desejada
const int delay = 1000.0f / FPS; //Divite 1000ms pela quantidade de frames desejada

//Objeto Jogo
alphaR * g_alphaR = 0;
Uint32 frameStart, frameTime;

int main(int argc, char **argv){
	alphaR::Instancia()->init("alphaR", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, alphaR::Instancia()->ObterH(), alphaR::Instancia()->ObterW(), SDL_WINDOW_SHOWN);

	while (alphaR::Instancia()->Rodando()){
		frameStart = SDL_GetTicks(); //Salva a quantidade de tempo (em milisegundos) desde que o SDL_Init foi chamado

		alphaR::Instancia()->HandleEvents();
		alphaR::Instancia()->Render();
		alphaR::Instancia()->Update();

		frameTime = SDL_GetTicks() - frameStart; //Calcula o tempo que o loop levou para ser executado
		
		if (frameTime < delay) //Se o tempo de execu��o for menor do que o delay desejado
			SDL_Delay((int)(delay - frameTime)); //Subtrai o tempo de atraso do fps desejado
	}

	alphaR::Instancia()->Clean();

	return 0;
}