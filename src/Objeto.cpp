#include "Objeto.h"
#include "Textura.h"
#include "alphaR.h"

void Objeto::Carregar(std::unique_ptr<Parametros> const &P){
	//Preenche os membros com os valores declarados no par�metro
	m_Posicao = Vetor2D(P->ObterX(), P->ObterY());
	m_Velocidade = Vetor2D(0, 0);
	m_Aceleracao = Vetor2D(0, 0);
	m_W = P->ObterW();
	m_H = P->ObterH();
	m_Frame = P->ObterFrame();
	m_AnimSpeed = P->ObterAnimSpeed();
	m_NumFrames = P->ObterNumFrames();
	m_Coluna = P->ObterColuna();
	m_TexturaID = P->ObterTexturaID();
	m_Angulo = P->ObterAngulo();
	m_pPonto = P->ObterPonto();
	m_Flip = P->ObterFlip();
	m_Alpha = P->ObterAlpha();
}

void Objeto::Desenhar(){
	Textura::Instancia()->DesenharFrame(m_TexturaID, alphaR::Instancia()->GetRenderer(), (Uint32)m_Posicao.ObterX(), (Uint32)m_Posicao.ObterY(), m_H, m_W, m_Frame, m_Coluna, m_Angulo, m_Alpha, m_pPonto, m_Flip);
}

void Objeto::Atualizar(){
}

void Objeto::Limpar(){
	Textura::Instancia()->Apagar(m_TexturaID);
}
