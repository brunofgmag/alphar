#include "Jogadores.h"
#include "alphaR.h"

Jogadores::Jogadores() : Carros(){

}

void Jogadores::Carregar(std::unique_ptr<Parametros> const &P){
	Carros::Carregar(P);
}

void Jogadores::Desenhar(){
	Carros::Desenhar();
}

void Jogadores::Atualizar(){
	if (m_TexturaID == std::string("carrovermelho")){
		m_FatAcel = 0.10;
		m_FatFre = 0.10;
	}
	else if (m_TexturaID == std::string("carroazul")){
		m_FatAcel = 0.05;
		m_FatFre = 0.20;
	}
	else if (m_TexturaID == std::string("carroverde")){
		m_FatAcel = 0.20;
		m_FatFre = 0.05;
	}

	if (!m_bIniciado){ //Para setar angulo inicial do objeto
		m_AnguloTemp = m_Angulo;
		m_bIniciado = true;
	}

	//Seta a escala X e Y do vetor baseado no angulo no qual o mesmo est� apontando
	m_AnguloTemp = m_Angulo / (180 / PI);
	m_EscalaY = cos(m_AnguloTemp);
	m_EscalaX = sin(m_AnguloTemp);

	//Faz o carro girar entre 0 e 360 graus
	if (m_Angulo > 359)
		m_Angulo = 0;
	else if (m_Angulo < 0)
		m_Angulo = 359;

	if (m_bMovendo){ //Se h� movimento atualiza a posicao com a velocidade
		m_Posicao += m_Velocidade;
	}
	else{ //Caso contr�rio zera qualquer movimento
		m_Velocidade.SetarX(0);
		m_Velocidade.SetarY(0);
		m_Aceleracao.SetarX(0);
		m_Aceleracao.SetarY(0);
	}

	//Evitando que o carro saia da tela
	if (m_Posicao.ObterX() + m_H / 1.3 >= alphaR::Instancia()->ObterH()){
		m_Posicao.SetarX(alphaR::Instancia()->ObterH() - m_H / 1.3);
		m_Modulo = 0.1; //Desacelera o carro ao bater
	}
	if (m_Posicao.ObterX() - m_W / 2 <= 0){
		m_Posicao.SetarX(m_W / 2);
		m_Modulo = 0.1; //Desacelera o carro ao bater
	}
	if (m_Posicao.ObterY() <= 0){
		m_Posicao.SetarY(0);
		m_Modulo = 0.1; //Desacelera o carro ao bater
	}
	if (m_Posicao.ObterY() + m_H >= alphaR::Instancia()->ObterW()){
		m_Posicao.SetarY(alphaR::Instancia()->ObterW() - m_H);
		m_Modulo = 0.1; //Desacelera o carro ao bater
	}

	//Calcula a movimenta��o do vetor de acordo com o angulo que o mesmo estiver apontando (F�RMULA ANTIGA)
	//m_Posicao.SetarX(m_Posicao.ObterX() - (-m_Velocidade.Comprimento() * sin((m_AnguloTemp * PI / 180))));
	//m_Posicao.SetarY(m_Posicao.ObterY() - (m_Velocidade.Comprimento() * cos((m_AnguloTemp * PI / 180))));

	//Passa o valor do angulo temporario para o angulo da imagem (F�RMULA ANTIGA)
	//m_Angulo = m_AnguloTemp;

	//Imprime na tela valores do modulo e aceleracao Y (Apenas para debug)
	//SDL_Log("%lf", m_Velocidade.Comprimento());
	//SDL_Log("%0.2f", m_Posicao.ObterX() - 21);

	Carros::Atualizar();
}

void Jogadores::Colisao(){
	if (m_Velocidade.ObterX() > 0){
		m_Posicao.SetarX(m_Posicao.ObterX() - m_Velocidade.ObterX() - 0.0001);
		m_Modulo = 0;
		m_bMovendo = false;
	}
	else{
		m_Posicao.SetarX(m_Posicao.ObterX() - m_Velocidade.ObterX() + 0.0001);
		m_Modulo = 0;
		m_bMovendo = false;
	}
	if (m_Velocidade.ObterY() < 0){
		m_Posicao.SetarY(m_Posicao.ObterY() - m_Velocidade.ObterY() + 0.0001);
		m_Modulo = 0;
		m_bMovendo = false;
	}
	else{
		m_Posicao.SetarY(m_Posicao.ObterY() - m_Velocidade.ObterY() - 0.0001);
		m_Modulo = 0;
		m_bMovendo = false;
	}
}

void Jogadores::Checkpoints(std::string ID){
	m_CheckID = std::atoi(ID.c_str());

	switch (m_CheckID)
	{
	case 0:
		if (m_LastCheck == 7 && m_NextCheck == 0 && m_Checkpoint == 0){
			m_LastCheck = 0;
			m_NextCheck = 1;
			m_Checkpoint++;
		}
		else if (m_Checkpoint == 7){
			m_LastCheck = 0;
			m_NextCheck = 1;
			m_Checkpoint = 1;
			m_NumVoltas++;
		}
		break;
	case 1:
		if (m_LastCheck == 0 && m_NextCheck == 1){
			m_LastCheck = 1;
			m_NextCheck = 2;
			m_Checkpoint++;
		}
		break;
	case 2:
		if (m_LastCheck == 1 && m_NextCheck == 2){
			m_LastCheck = 2;
			m_NextCheck = 3;
			m_Checkpoint++;
		}
		break;
	case 3:
		if (m_LastCheck == 2 && m_NextCheck == 3){
			m_LastCheck = 3;
			m_NextCheck = 4;
			m_Checkpoint++;
		}
		break;
	case 4:
		if (m_LastCheck == 3 && m_NextCheck == 4){
			m_LastCheck = 4;
			m_NextCheck = 5;
			m_Checkpoint++;
		}
		break;
	case 5:
		if (m_LastCheck == 4 && m_NextCheck == 5){
			m_LastCheck = 5;
			m_NextCheck = 6;
			m_Checkpoint++;
		}
		break;
	case 6:
		if (m_LastCheck == 5 && m_NextCheck == 6){
			m_LastCheck = 6;
			m_NextCheck = 7;
			m_Checkpoint++;
		}
		break;
	case 7:
		if (m_LastCheck == 6 && m_NextCheck == 7){
			m_LastCheck = 7;
			m_NextCheck = 0;
			m_Checkpoint++;
		}
		break;
	default: SDL_Log("Erro nao calculado! (Checkpoints)");
		break;
	}

	//Para debug
	//SDL_Log("Checkpoint: %d - Volta: %d - ID: %d - Last: %d - Next: %d", m_Checkpoint, m_NumVoltas, m_CheckID, m_LastCheck, m_NextCheck);
}

void Jogadores::Limpar(){
	Carros::Limpar();
}