#ifndef __TextosDinamicos__
#define __TextosDinamicos__

#include "Objeto.h"
#include "ManipuladorObjetos.h"

class TextosDinamicos : public Objeto{
public:
	TextosDinamicos();
	~TextosDinamicos() {}

	void Carregar(std::unique_ptr<Parametros> const &P, std::string Texto, int Tamanho, std::string Fonte, int R, int G, int B);
	void Desenhar();
	void Atualizar();
	void Limpar();
	void SetarTexto(std::string ID, std::string Texto);
	std::string Tipo() { return "TextosDinamicos"; }

private:
	std::string m_Texto;
	std::string m_Fonte;
	int m_Tamanho;
	int m_R;
	int m_G;
	int m_B;
	std::vector<std::string> m_IDs;
};

class CriadorTextosDinamicos : public CriadorBase{
	Objeto * CriarObjeto() const{
		return new TextosDinamicos();
	}
};


#endif