#ifndef __JogadorUm__
#define __JogadorUm__

#include "Jogadores.h"
#include "ManipuladorEntrada.h"
#include "ManipuladorObjetos.h"

class JogadorUm : public Jogadores{
public:
	JogadorUm();
	~JogadorUm() {}

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();
	void Colisao();
	void Checkpoints(std::string ID);

private:
	void EntradaDados(); //Gerencia a entrada de dados do teclado
};

class CriadorJogadorUm : public CriadorBase{
	Jogadores * CriarObjeto() const{
		return new JogadorUm();
	}
};

#endif