#ifndef __ManipuladorColisoes__
#define __ManipuladorColisoes__

#include "CamadaTile.h"
#include "Jogadores.h"
#include "Objeto.h"
#include "Colisoes.h"

class ManipuladorColisoes{
public:
	void ChecarJogadorTileColisao(Jogadores *pJogadores, const std::vector<CamadaTile*> ColisoesCamadas);
	void ChecarJogadorObjetoColisao(Jogadores *pJogadores, const std::vector<Objeto*> &objetos);
	void ChecarCheckpoint(Jogadores *pJogadores, const std::vector<Objeto*> &objetos);

private:
	Colisoes m_Colisao;
};

#endif