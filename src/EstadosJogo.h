#ifndef __EstadosJogo__
#define __EstadosJogo__

#include <string>
#include <vector>

class TextosDinamicos;

class EstadosJogo{ //Classe abstrata EstadosJogo
public:
	virtual void Atualizar() = 0;
	virtual void Desenhar() = 0;

	virtual bool AoEntrar() = 0;
	virtual bool AoSair() = 0;

	virtual std::string ObterID() const = 0;

protected:
	std::vector<std::string> m_TexturasID;
};
#endif