#include "ManipuladorEstados.h"
#include "SDL.h"

void ManipuladorEstados::ChamarEstado(EstadosJogo * pEstado){
	m_EstadosJogo.push_back(pEstado);
	m_EstadosJogo.back()->AoEntrar();
}

void ManipuladorEstados::MudarEstado(EstadosJogo * pEstado){
	if (!m_EstadosJogo.empty()) { //Verifica se a array n�o est� vazia
		if (m_EstadosJogo.back()->ObterID() == pEstado->ObterID()){ //Verifica se o estado anterior � igual ao estado atual
			return; //N�o faz nada
		}

		if (m_EstadosJogo.back()->AoSair()){ //Se o estado anterior sair com sucesso
			m_EstadosJogo.pop_back();
		}
	}

	//Coloca o novo estado na array
	m_EstadosJogo.push_back(pEstado);

	//Inicia o Estado
	m_EstadosJogo.back()->AoEntrar();
}

void ManipuladorEstados::RemoverEstado(){
	if (!m_EstadosJogo.empty()) { //Verifica se a array n�o est� vazia
		if (m_EstadosJogo.back()->AoSair()){ //Verifica se ultimo estado da array saiu com sucesso
			m_EstadosJogo.pop_back(); //Remove o estado da array
		}
	}

}

void ManipuladorEstados::Atualizar(){
	if (!m_EstadosJogo.empty()){
		m_EstadosJogo.back()->Atualizar();
	}
}

void ManipuladorEstados::Desenhar(){
	if (!m_EstadosJogo.empty()){
		m_EstadosJogo.back()->Desenhar();
	}
}

void ManipuladorEstados::Limpar(){
	if (!m_EstadosJogo.empty()){
		m_EstadosJogo.back()->AoSair();
		delete m_EstadosJogo.back();
		m_EstadosJogo.clear();
		SDL_Log("Todos os estados foram apagados com sucesso!");
	}
}