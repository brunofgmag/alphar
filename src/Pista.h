#ifndef __Pista__
#define __Pista__

#include <string>
#include <vector>
#include "Camadas.h"
#include "AnalisadorPista.h"

class JogadorUm;
class JogadorDois;
class Objeto;

struct Tileset //Obt�m todas as informa��es necess�rias para os tilesets
{
	int PrimeiroGridID;
	int TileW;
	int TileH;
	int Espacamento;
	int Margem;
	int W;
	int H;
	int NumColunas;
	std::string Nome;
};

class Pista{
public:
	~Pista() {}

	void Desenhar();
	void Atualizar();
	void Limpar();
	JogadorUm *ObterJogadorUm(){ return m_pJogadorUm; }
	JogadorDois *ObterJogadorDois(){ return m_pJogadorDois; }
	void SetarJogadorUm(JogadorUm *pJogador){ m_pJogadorUm = pJogador; }
	void SetarJogadorDois(JogadorDois *pJogador){ m_pJogadorDois = pJogador; }

	std::vector<Tileset> *ObterTilesets(){ return &m_Tilesets; }
	std::vector<Camadas*> *ObterCamadas(){ return &m_Camadas; }
	std::vector<Objeto*> &ObterObjetosColisiveis(){ return m_pObjetosColisiveis; }
	std::vector<Objeto*> *ObterObjetosColisao(){ return &m_pObjetosColisiveis; }
	std::vector<Objeto*> *ObterObjetosCheckpoint() { return &m_pObjetosCheckpoints; }
	std::vector<Objeto*> &ObterObjetosCheckpoints() { return m_pObjetosCheckpoints; }
	std::vector<CamadaTile*> *ObterCamadasColisao(){ return &m_CamadasColisiveis; }
	const std::vector<CamadaTile*> &ObterCamadaColisivel(){ return m_CamadasColisiveis; }

private:
	friend class AnalisadorPista; //Dando acesso ao construtor privado de Pista
	Pista() {}
	std::vector<Camadas*> m_Camadas;
	std::vector<Tileset> m_Tilesets;
	std::vector<CamadaTile*> m_CamadasColisiveis;
	std::vector<Objeto*> m_pObjetosColisiveis;
	std::vector<Objeto*> m_pObjetosCheckpoints;
	JogadorDois *m_pJogadorDois;
	JogadorUm *m_pJogadorUm;
};

#endif