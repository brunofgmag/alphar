#include "AnalisadorEstados.h"
#include "TextosDinamicos.h"
#include "alphaR.h"

bool AnalisadorEstados::AnalisarEstado(const char * ArquivoEstado, std::string EstadoID, std::vector<Objetos*> *pObjetos, std::vector<std::string> *TexturasIDs){
	//Cria o arquivo XML
	TiXmlDocument xmlDoc;

	//Carrega o arquivo dos Estados
	if (!xmlDoc.LoadFile(ArquivoEstado)){
		SDL_Log("%s \n", xmlDoc.ErrorDesc());
		return false;
	}

	//Obter Elemento raiz
	TiXmlElement * pRaiz = xmlDoc.RootElement(); // <ESTADOS>

	// Pr�-declara o n� da raiz dos Estados
	TiXmlElement * pEstatoRaiz = 0;

	//Pega esse n� e atribui a pEstadoRaiz
	for (TiXmlElement *e = pRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == EstadoID){
			pEstatoRaiz = e;
		}
	}

	//Pr�-declara o n� da raiz das Texturas
	TiXmlElement * pTexturaRaiz = nullptr;

	//Pega esse n� e atribui a pTexturaRaiz
	for (TiXmlElement *e = pEstatoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("TEXTURAS")){
			pTexturaRaiz = e;
		}
	}

	//Analisar as texturas
	AnalisarTexturas(pTexturaRaiz, TexturasIDs);

	//Pr�-declara o n� da raiz dos Objetos
	TiXmlElement * pObjetoRaiz = nullptr;

	//Pega esse n� e atribui a pObjetoRaiz
	for (TiXmlElement *e = pEstatoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("OBJETOS")){
			pObjetoRaiz = e;
		}
	}

	//Analisar os objetos
	AnalisarObjetos(pObjetoRaiz, pObjetos);

	//Pr�-declara o n� da raiz dos Textos
	TiXmlElement * pTextoRaiz = nullptr;

	//Pega esse n� e atribui a pTextoRaiz
	for (TiXmlElement *e = pEstatoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("TEXTOS")){
			pTextoRaiz = e;
		}
	}

	//Analisar os textos
	CarregarTexto(pTextoRaiz, pObjetos, TexturasIDs);

	//Pr�-declara o n� da raiz dos Textos Dinamicos
	TiXmlElement * pTextoDinamicoRaiz = nullptr;

	//Pega esse n� e atribui a pTextoDinamicoRaiz
	for (TiXmlElement *e = pEstatoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		if (e->Value() == std::string("TEXTOSDINAMICOS")){
			pTextoDinamicoRaiz = e;
		}
	}

	//Analisar os Textos Dinamicos
	CarregarTextoDinamico(pTextoDinamicoRaiz, pObjetos, TexturasIDs);
	
	return true;
}

void AnalisadorEstados::AnalisarTexturas(TiXmlElement * pEstadoRaiz, std::vector<std::string> *TexturasIDs){
	for (TiXmlElement *e = pEstadoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		std::string AtributoArquivo = e->Attribute("arquivo");
		std::string AtribuitoID = e->Attribute("ID");

		TexturasIDs->push_back(AtribuitoID); //Adiciona no vector

		Textura::Instancia()->Carregar(AtributoArquivo, AtribuitoID, alphaR::Instancia()->GetRenderer());
	}
}

void AnalisadorEstados::AnalisarObjetos(TiXmlElement * pEstadoRaiz, std::vector<Objetos*> *pObjetos){
	for (TiXmlElement *e = pEstadoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		int x, y, h, w, frame = 0, numFrames = 0, animSpeed = 0, coluna = 0, callbackID = 0, flip = 0, alpha = 255;
		double angulo = 0.0;
		std::string TexturaID;

		e->Attribute("x", &x);
		e->Attribute("y", &y);
		e->Attribute("h", &h);
		e->Attribute("w", &w);
		e->Attribute("frame", &frame);
		e->Attribute("numFrames", &numFrames);
		e->Attribute("animSpeed", &animSpeed);
		e->Attribute("coluna", &coluna);
		e->Attribute("angulo", &angulo);
		e->Attribute("flip", &flip);
		e->Attribute("callbackID", &callbackID);
		e->Attribute("alpha", &alpha);

		TexturaID = e->Attribute("texturaID");

		switch (flip){
		case 1:
			m_Flip = SDL_FLIP_HORIZONTAL;
			break;
		case 2:
			m_Flip = SDL_FLIP_VERTICAL;
			break;
		default:
			m_Flip = SDL_FLIP_NONE;
		}

		Objetos * pObjeto = ManipuladorObjetos::Instancia()->Criar(e->Attribute("tipo"));
		pObjeto->Carregar(std::unique_ptr<Parametros>(new Parametros(TexturaID, x, y, h, w, frame, numFrames, animSpeed, coluna, angulo, 0, m_Flip, callbackID, alpha)));
		pObjetos->push_back(pObjeto);
	}
}

void AnalisadorEstados::CarregarTexto(TiXmlElement * pEstadoRaiz, std::vector<Objetos*> *pObjetos, std::vector<std::string> *TexturasIDs){
	for (TiXmlElement *e = pEstadoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		int Tamanho = 0, R = 255, G = 255, B = 255, x = 0, y = 0, w = 0, h = 0;
		if (e->Value() == std::string("texto") && e != nullptr){
			std::string Fonte = e->Attribute("fonte");
			std::string Texto = e->Attribute("texto");
			std::string ID = e->Attribute("ID");
			e->Attribute("tamanho", &Tamanho);
			e->Attribute("R", &R);
			e->Attribute("G", &G);
			e->Attribute("B", &B);
			e->Attribute("x", &x);
			e->Attribute("y", &y);
			e->Attribute("h", &h);
			e->Attribute("w", &w);

			SDL_Color Cor = { R, G, B };

			TexturasIDs->push_back(ID); //Adiciona no vector

			Textura::Instancia()->CarregarTexto(Fonte, ID, alphaR::Instancia()->GetRenderer(), Tamanho, Texto, Cor);
			Objetos *pTexto = ManipuladorObjetos::Instancia()->Criar("ImagemEstatica");
			pTexto->Carregar(std::unique_ptr<Parametros>(new Parametros(ID, x, y, h, w)));
			pObjetos->push_back(pTexto);
		}
	}
}

void AnalisadorEstados::CarregarTextoDinamico(TiXmlElement * pEstadoRaiz, std::vector<Objetos*> *pObjetos, std::vector<std::string> *TexturasIDs){
	for (TiXmlElement *e = pEstadoRaiz->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
		int Tamanho = 0, R = 255, G = 255, B = 255, x = 0, y = 0, w = 0, h = 0;
		if (e->Value() == std::string("textodinamico") && e != nullptr){
			std::string Fonte = e->Attribute("fonte");
			std::string Texto = e->Attribute("texto");
			std::string ID = e->Attribute("ID");
			e->Attribute("tamanho", &Tamanho);
			e->Attribute("R", &R);
			e->Attribute("G", &G);
			e->Attribute("B", &B);
			e->Attribute("x", &x);
			e->Attribute("y", &y);
			e->Attribute("h", &h);
			e->Attribute("w", &w);

			SDL_Color Cor = { R, G, B };

			TexturasIDs->push_back(ID); //Adiciona no vector

			TextosDinamicos *pTextoDinamico = nullptr;
			Textura::Instancia()->CarregarTexto(Fonte, ID, alphaR::Instancia()->GetRenderer(), Tamanho, Texto, Cor);
			Objetos *pTexto = ManipuladorObjetos::Instancia()->Criar("TextoDinamico");
			pTextoDinamico = dynamic_cast<TextosDinamicos*>(pTexto);
			pTextoDinamico->Carregar(std::unique_ptr<Parametros>(new Parametros(ID, x, y, h, w)), Texto, Tamanho, Fonte, R, G, B);
			pObjetos->push_back(pTextoDinamico);
		}
	}
}