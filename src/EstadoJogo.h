#ifndef __EstadoJogo__
#define __EstadoJogo__

#include "Objetos.h"
#include "EstadosJogo.h"
#include "Pista.h"
#include <vector>
#include <sstream>

class EstadoJogo : public EstadosJogo{ //Classe que define o estado de jogo Menu
public:
	void Atualizar();
	void Desenhar();
	void teste();

	bool AoEntrar();
	bool AoSair();

	std::string ObterID() const { return s_JogoID; }
	void CalcularHora();

private:
	static const std::string s_JogoID;

	Pista * pPista;
	std::vector<Objetos*> m_pObjeto;
	
	//Atributos de controle
	bool m_bPausado = false;
	bool m_bGameOver = false;
	int m_TempoBuffer = -1; //Buffer para atualiza��o do tempo e texto somente a cada mudan�a de segundo
	int m_VoltaP1Buffer = -1;
	int m_VoltaP2Buffer = -1;
	int m_Vencedor = 0;

	//Texto Dinamico
	TextosDinamicos *m_pTextosDinamicos;

	//Jogadores
	JogadorUm *m_pJogadorUm = nullptr;
	JogadorDois *m_pJogadorDois = nullptr;
	std::string m_JogadorUmNome;
	std::string m_JogadorDoisNome;
	std::string m_VoltasJogadorUm;
	std::string m_VoltasJogadorDois;
	std::string m_Vencedor1 = "O jogador 1 venceu!! Aperte ESC para jogar novamente ou sair do jogo!";
	std::string m_Vencedor2 = "O jogador 2 venceu!! Aperte ESC para jogar novamente ou sair do jogo!";

	//Tempo
	long int m_Segundo = 0;
	long int m_Milisegundos = 0;
	int m_Minuto = 0;
	std::string m_Tempo[2];
	std::stringstream StringStream;
	std::string m_S;
	const char *m_pTempo;
	const char *m_pTempoLast;

	//Ticks
	Uint32 m_TickInicial = 0;
	Uint32 m_TickReal = 0;
	Uint32 m_TickPausa = 0;
	Uint32 m_TickRetorno = 0;
};

#endif