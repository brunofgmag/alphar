#ifndef __CamadaObjeto__
#define __CamadaObjeto__

#include <vector>
#include "Objetos.h"
#include "Camadas.h"
#include "ManipuladorColisoes.h"

class Pista;

class CamadaObjeto : public Camadas{
public:
	void Desenhar();
	void Atualizar(Pista *pPista);
	void Limpar();

	std::vector<Objetos*> *ObterObjetos(){
		return &m_Objetos;
	}

private:
	std::vector<Objetos*> m_Objetos;
	ManipuladorColisoes m_Colisoes;
	JogadorUm *m_pJogadorUm;
};

#endif