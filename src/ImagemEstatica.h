#ifndef __ImagemEstatica__
#define __ImagemEstatica__

#include "Parametros.h"
#include "Objeto.h"
#include "ManipuladorObjetos.h"

class ImagemEstatica : public Objeto{
public:
	ImagemEstatica();

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();
};

class CriadorImagemEstatica : public CriadorBase{
	Objeto * CriarObjeto() const{
		return new ImagemEstatica();
	}
};


#endif