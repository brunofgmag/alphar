#include "EstadoPausa.h"
#include "alphaR.h"


const std::string EstadoPausa::s_PausaID = "PAUSA";

void EstadoPausa::Atualizar(){
	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Atualizar();
	}
}

void EstadoPausa::Desenhar(){
	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Desenhar();
	}
}

bool EstadoPausa::AoEntrar(){
	AnalisadorEstados analisadorEstados;
	analisadorEstados.AnalisarEstado("res/Estados.xml", s_PausaID, &m_pObjeto, &m_TexturasID);
	m_Callbacks.push_back(s_PausaParaJogo);
	m_Callbacks.push_back(s_PausaParaMenu);

	//Seta os callbacks para os itens do menu
	SetarCallbacks(m_Callbacks);
	SDL_Log("Entrando no estado PAUSA");

	return true;
}

void EstadoPausa::SetarCallbacks(const std::vector<Callback>& callbacks){
	if (!m_pObjeto.empty()){
		//Loop entre os objetos
		for (int i = 0; i < m_pObjeto.size(); i++){
			//Se eles forem do tipo BotaoMenu entao ser� designado um callback baseado no ID do arquivo XML
			if (dynamic_cast<BotaoMenu*>(m_pObjeto[i])){
				BotaoMenu * pBotao = dynamic_cast<BotaoMenu*>(m_pObjeto[i]);
				pBotao->SetarCallback(callbacks[pBotao->ObterCallbackID()]);
			}
		}
	}
}

bool EstadoPausa::AoSair(){
	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Limpar();
	}

	m_pObjeto.clear();
	ManipuladorEntrada::Instancia()->Resetar(); //Reseta o estado dos botoes do mouse para falso
	SDL_Log("Saindo do estado PAUSA");
	return true;
}

void EstadoPausa::s_PausaParaJogo(){
	alphaR::Instancia()->ObterEstado()->RemoverEstado();
}

void EstadoPausa::s_PausaParaMenu(){
	alphaR::Instancia()->ObterEstado()->RemoverEstado();
	alphaR::Instancia()->ObterEstado()->MudarEstado(new EstadoMenu());
}