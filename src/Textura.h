#ifndef __Textura__
#define __Textura__

#include <string>
#include <map>
#include "SDL.h"

class Textura{
public:
	//Instanciador para evitar que a classe seja criada mais de uma vez
	static Textura * Instancia(){
		if (s_pInstancia == nullptr)
			s_pInstancia = new Textura;
		return s_pInstancia;
	}

	//Cria um vetor que associa e armazena uma vari�vel do tipo String a um ponteiro para um objeto do tipo SDL_Texture
	std::map<std::string, SDL_Texture*> m_pTexturas;

	//M�todos da classe
	bool Carregar(std::string Arquivo, std::string ID, SDL_Renderer * Renderer);
	bool CarregarTexto(std::string Fonte, std::string ID, SDL_Renderer * Renderer, int Tamanho, std::string Texto, SDL_Color Cor);
	void Desenhar(std::string ID, SDL_Renderer * Renderer, int X, int Y, int H, int W);
	void DesenharFrame(std::string ID, SDL_Renderer * Renderer, int X, int Y, int H, int W, int Frame, int Coluna, double Angulo = 0, int Alpha = 255, SDL_Point * Ponto = 0, SDL_RendererFlip Flip = SDL_FLIP_NONE);
	void DesenharTile(std::string ID, int Margem, int Espacamento, int X, int Y, int W, int H, int Linha, int Frame, SDL_Renderer * pRenderer);
	void Apagar(std::string ID);
	void Limpar();

private:
	Textura() {}
	~Textura() {}

	static Textura * s_pInstancia; //Declara o ponteiro do objeto est�tico Instancia

	//Objetos dos ret�ngulos
	SDL_Rect m_Origem;
	SDL_Rect m_Destino;
};

#endif