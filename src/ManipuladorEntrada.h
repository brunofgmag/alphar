#ifndef __ManipuladorEntrada__
#define __ManipuladorEntrada__

#include "SDL.h"
#include "Vetor2D.h"
#include <vector>

enum BotoesMouse {
	LEFT = 0,
	MIDDLE = 1,
	RIGHT = 2
};

class ManipuladorEntrada{
public:
	//Instanciador para evitar que a classe seja criada mais de uma vez
	static ManipuladorEntrada * Instancia(){
		if (s_pInstancia == nullptr)
			s_pInstancia = new ManipuladorEntrada;
		return s_pInstancia;
	}

	//Declarando m�todos
	void Atualizar();
	void Resetar();
	bool TeclaPressionada(SDL_Scancode Tecla) const;

	//Eventos do Mouse
	bool ObterBotaoMouse(int NumeroBotao) const{
		return m_EstadoBotao[NumeroBotao];
	}

	Vetor2D PosicaoMouse()
	{
		return m_PosicaoMouse;
	}
	
private:
	ManipuladorEntrada();
	~ManipuladorEntrada();

	static ManipuladorEntrada * s_pInstancia; //Declara o ponteiro do objeto est�tico Instancia

	//Eventos de teclado
	void OnTeclaPressionada();
	void OnTeclaSolta();

	const Uint8 * m_EstadoTecla;

	//Eventos de mouse
	void MouseMovimento(SDL_Event &Evento);
	void MouseBotaoPressionado(SDL_Event &Evento);
	void MouseBotaoSolto(SDL_Event &Evento);

	std::vector<bool> m_EstadoBotao;
	Vetor2D m_PosicaoMouse;
};

#endif