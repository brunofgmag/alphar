#ifndef __Vetor2D__
#define __Vetor2D__

#include <math.h>

class Vetor2D{ //Classe para trabalhar com objetos do tipo vetor (sistema cartesiano)
public:
	Vetor2D(){ //Construtor da classe com atribui��o de valores
		m_X = 0;
		m_Y = 0;
	}

	Vetor2D(float x, float y) : m_X(x), m_Y(y) {} //Construtor da classe com atribui��o de valores

	//Declarando m�todos
	float ObterX() { return m_X; }
	float ObterY() { return m_Y; }

	float Comprimento() { float temp = sqrt((m_X * m_X) + (m_Y * m_Y)); return sqrt(pow(m_X, 2) + pow(m_Y, 2)); }

	void SetarX(float x) { m_X = x; }
	void SetarY(float y) { m_Y = y; }

	//Atribuindo operadores para trabalhar com opera��o de vetores

	Vetor2D & operator+(const Vetor2D &v2) const{ //Definindo o operador de soma para soma de vetores
		return Vetor2D(m_X + v2.m_X, m_Y + v2.m_Y);
	}

	friend Vetor2D & operator+=(Vetor2D &v1, const Vetor2D &v2){ //Definindo o operador de aci��o para soma de vetores (usando friend para acessar membros privados)
		v1.m_X += v2.m_X;
		v1.m_Y += v2.m_Y;

		return v1;
	}

	Vetor2D & operator*(float scalar){ //Definindo o operador de multiplica��o para multiplicar vetores
		return Vetor2D(m_X * scalar, m_Y * scalar);
	}

	Vetor2D & operator*=(float scalar){ //Definindo o operador de multiplica��o para multiplicar vetores
		m_X *= scalar;
		m_Y *= scalar;

		return * this; //Usando apontador this para retornar o ponteiro do objeto
	}

	Vetor2D & operator-(const Vetor2D &v2) const{ //Definindo o operator de subtra��o para subtrair vetores
		return Vetor2D(m_X - v2.m_X, m_Y - v2.m_Y);
	}

	friend Vetor2D & operator-=(Vetor2D &v1, const Vetor2D &v2){ //Definindo o operator de subtra��o para subtrair vetores (usando friend para acessar membros privados)
		v1.m_X -= v2.m_X;
		v1.m_Y -= v2.m_Y;

		return v1;
	}

	Vetor2D & operator/(float scalar){ //Definindo o operator de divis�o para dividir vetores
		return Vetor2D(m_X / scalar, m_Y / scalar);
	}

	Vetor2D & operator/=(float scalar){ //Definindo o operador de divis�o para dividir vetores
		m_X /= scalar;
		m_Y /= scalar;

		return * this; //Usando apontador this para retornar o ponteiro do objeto
	}

	void Normalizar(){ //M�todo de normalizar o vetor
		float C = Comprimento();
		if (C > 0) //Evita divis�es por zero
		{
			(*this) *= 1 / C;
		}
	}

private:
	float m_X;
	float m_Y;
};

#endif