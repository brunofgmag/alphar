#include "alphaR.h"

alphaR * alphaR::s_pInstancia = 0; //Zera o contador de instancias

bool alphaR::init(const char * Titulo, int PosX, int PosY, int Largura, int Altura, int Flags){
	//Iniciar SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0){
		SDL_Log("Inicializado com sucesso\n");
		m_pJanela = SDL_CreateWindow(Titulo, PosX, PosY, Largura, Altura, Flags);

		//Inicia biblioteca de imagens
		if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)){
			SDL_Log("Nao foi possivel iniciar a biblioteca de imagens: %s", IMG_GetError()); //Mostra erro (caso exista)
		}

		//Inicia biblioteca de fontes TTF
		if (TTF_Init() == -1){
			SDL_Log("Nao foi possivel iniciar a biblioteca de fontes TTF: %s", TTF_GetError()); //Mostra erro (caso exista)
		}

		//Inicia o renderizador
		if (m_pJanela != 0){
			SDL_Log("Janela criada com sucesso\n");
			m_pRenderer = SDL_CreateRenderer(m_pJanela, -1, SDL_RENDERER_ACCELERATED);

			if (m_pRenderer != 0){
				SDL_Log("Renderizador inicializado com sucesso\n");
				SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 255);
			}
			else{
				SDL_Log("Nao foi possivel iniciar o renderizador: %s", SDL_GetError()); //Mostra erro (caso exista)
			}
		}
		else{
			SDL_Log("Nao foi possivel criar a janela: %s", SDL_GetError()); //Mostra erro (caso exista)
		}
	}
	else{
		SDL_Log("Nao foi possivel iniciar o SDL: %s", SDL_GetError()); //Mostra erro (caso exista)
	}
	//Criando os objetos
	ManipuladorObjetos::Instancia()->RegistrarTipo("BotaoMenu", new CriadorBotaoMenu());
	ManipuladorObjetos::Instancia()->RegistrarTipo("ImagemEstatica", new CriadorImagemEstatica());
	ManipuladorObjetos::Instancia()->RegistrarTipo("JogadorDois", new CriadorJogadorDois());
	ManipuladorObjetos::Instancia()->RegistrarTipo("JogadorUm", new CriadorJogadorUm());
	//ManipuladorObjetos::Instancia()->RegistrarTipo("CarroAzul", new CriadorCarroAzul());
	ManipuladorObjetos::Instancia()->RegistrarTipo("GraficoAnimado", new CriadorGraficoAnimado());
	ManipuladorObjetos::Instancia()->RegistrarTipo("TextoDinamico", new CriadorTextosDinamicos());

	//Estados do jogo
	m_Estados = new ManipuladorEstados();
	m_Estados->MudarEstado(new EstadoMenu());

	m_Rodando = true; //Inicia o loop
	return true;
}

void alphaR::Render(){
	//Limpa a tela aplicando a cor
	SDL_RenderClear(m_pRenderer);

	/*//Desenha os carros
	for (std::vector<Objeto*>::size_type i = 0; i != Carros.size(); i++)
	{
		Carros[i]->Desenhar();
	}*/

	//Desenha o estado atual
	m_Estados->Desenhar();

	//Mostra a janela
	SDL_RenderPresent(m_pRenderer);
}

void alphaR::Update(){
	/*if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_RETURN)){
		m_Estados->MudarEstado(new EstadoJogo());
	}

	if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_DELETE)){
		m_Estados->RemoverEstado();
	}*/

	//Atualiza o estados
	m_Estados->Atualizar();

	/*//Atualiza os carros
	for (std::vector<Objeto*>::size_type i = 0; i != Carros.size(); i++)
	{
		Carros[i]->Atualizar();
	}*/
}

void alphaR::HandleEvents(){
	ManipuladorEntrada::Instancia()->Atualizar();
}

void alphaR::Clean(){
	//Limpa os carros
	/*for (std::vector<Objeto*>::size_type i = 0; i != Carros.size(); i++)
	{
		Carros[i]->Limpar();
	}*/

	//Destr�i as texturas
	Textura::Instancia()->Limpar();

	//Destr�i os estados
	m_Estados->Limpar();

	//Destr�i a janela
	SDL_DestroyWindow(m_pJanela);

	//Destr�i o renderer
	SDL_DestroyRenderer(m_pRenderer);

	//Encerra a biblioteca de fontes TTF
	TTF_Quit();

	//Encerra a biblioteca de imagens
	IMG_Quit();

	//Fecha o aplicativo
	SDL_Quit();
	 
	//Finaliza o aplicativo
	m_Rodando = 0;
}