#ifndef __Objeto__
#define __Objeto__

#include "Objetos.h"

class Objeto : public Objetos{
public:
	~Objeto() {}
	Objeto() {}

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();
	void Colisao() {}
	std::string Tipo() { return "Objeto"; }
};

#endif