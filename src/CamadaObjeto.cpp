#include "CamadaObjeto.h"
#include "JogadorUm.h"
#include "JogadorDois.h"
#include "Pista.h"

void CamadaObjeto::Desenhar(){
	for (int i = 0; i < m_Objetos.size(); i++){
		m_Objetos[i]->Desenhar();
	}
}

void CamadaObjeto::Atualizar(Pista *pPista){
	m_Colisoes.ChecarJogadorTileColisao(pPista->ObterJogadorUm(), pPista->ObterCamadaColisivel());
	m_Colisoes.ChecarJogadorTileColisao(pPista->ObterJogadorDois(), pPista->ObterCamadaColisivel());
	m_Colisoes.ChecarJogadorObjetoColisao(pPista->ObterJogadorUm(), pPista->ObterObjetosColisiveis());
	m_Colisoes.ChecarJogadorObjetoColisao(pPista->ObterJogadorDois(), pPista->ObterObjetosColisiveis());
	m_Colisoes.ChecarCheckpoint(pPista->ObterJogadorUm(), pPista->ObterObjetosCheckpoints());
	m_Colisoes.ChecarCheckpoint(pPista->ObterJogadorDois(), pPista->ObterObjetosCheckpoints());

	for (int i = 0; i < m_Objetos.size(); i++){
		m_Objetos[i]->Atualizar();
	}
}

void CamadaObjeto::Limpar(){
	for (int i = 0; i < m_Objetos.size(); i++){
		m_Objetos[i]->Limpar();
	}
}