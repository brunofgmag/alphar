#ifndef __EstadosMenu__
#define __EstadosMenu__

#include "EstadosJogo.h"
#include <vector>

class EstadosMenu : public EstadosJogo{
protected:
	typedef void(*Callback)();
	virtual void SetarCallbacks(const std::vector<Callback>& callbacks) = 0;

	std::vector<Callback> m_Callbacks;
};


#endif