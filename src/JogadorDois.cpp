#include "JogadorDois.h"
#include "alphaR.h"
#include <iostream>
#include <math.h>

JogadorDois::JogadorDois() : Jogadores(){
	m_bRodando = false;
}

void JogadorDois::Carregar(std::unique_ptr<Parametros> const &P){
	Jogadores::Carregar(P);

	if (m_TexturaID == std::string("carronone")){
		printf("Digite o nome do Jogador 2: ");
		std::cin.clear();
		std::cin >> m_Nome;

		printf("Ola %s, por favor escolha seu carro:\n1- Carro Vermelho (Aceleracao: 1 - Frenagem: 1)\n2- Carro Azul (Aceleracao: 1/2 - Frenagem: 2)\n3- Carro Verde (Aceleracao: 2 - Frenagem: 1/2)\nOpcao: ", m_Nome.c_str());
		std::cin.clear();
		std::cin >> m_TipoCarro;

		switch (m_TipoCarro){
		case 1: m_TexturaID = "carrovermelho";
			break;
		case 2: m_TexturaID = "carroazul";
			break;
		case 3: m_TexturaID = "carroverde";
			break;
		default: m_TexturaID = "carrovermelho";
			break;
		}
	}
	m_bRodando = false;
}

void JogadorDois::Desenhar(){
	Jogadores::Desenhar();
}

void JogadorDois::Atualizar(){
	//Para debug
	//SDL_Log("x: %f -- y: %f -- mod: %f", m_Velocidade.ObterX(), m_Velocidade.ObterY(), m_Modulo);
	//SDL_Log("x: %f -- y: %f", m_Posicao.ObterX(), m_Posicao.ObterY());

	this->EntradaDados();

	Jogadores::Atualizar();
}

void JogadorDois::Limpar(){
	Jogadores::Limpar();
}

void JogadorDois::Colisao(){
	Jogadores::Colisao();
}

void JogadorDois::Checkpoints(std::string ID){
	Jogadores::Checkpoints(ID);
}

void JogadorDois::EntradaDados(){
	if (m_bRodando){
		if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_D)){ //Se a seta para a direita for pressionada
			if (m_Modulo != 0) //Verifica se h� movimento para fazer a curva
				m_Angulo += m_FatCurva;
		}
		if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_A)){ //Se a seta para a esquerda for pressionada
			if (m_Modulo != 0) //Verifica se h� movimento para fazer a curva
				m_Angulo -= m_FatCurva;
		}
		if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_W)){ //Se a seta para cima for pressionada
			if (m_bMovendo == false && m_Modulo != 0){
				m_MaxVeloc = 0;
			}

			m_bMovendo = true; //Coloca o carro em movimento

			//Se o m�dulo ultrapassar a velocidade m�xima, seta ela
			if (m_Modulo > m_MaxVeloc){
				m_Modulo = m_MaxVeloc;
			}

			//Se o M�dulo for menor ou igual a velocidade m�xima, efetua a acelera��o at� chegar a velocidade m�xima
			if (m_Modulo <= m_MaxVeloc){
				m_Aceleracao.SetarY(m_FatAcel * -m_EscalaY); //Seta a acelera��o
				m_Aceleracao.SetarX(m_FatAcel * m_EscalaX); //Seta a acelera��o
				m_Velocidade.SetarY(m_Modulo * -m_EscalaY); //Seta a velocidade
				m_Velocidade.SetarX(m_Modulo * m_EscalaX); //Seta a velocidade
				m_Modulo = m_Velocidade.Comprimento() + m_Aceleracao.Comprimento(); //Calcula o m�dulo com acelera��o positiva
			}
			else{ //Caso j� tenha chegado a velocidade m�xima, retira a acelera��o e passa a usar como referencia a velocidade m�xima
				m_Modulo = m_MaxVeloc;
				m_Aceleracao.SetarX(0);
				m_Aceleracao.SetarY(0);
				m_Velocidade.SetarY(m_Modulo * -m_EscalaY); //Seta a velocidade
				m_Velocidade.SetarX(m_Modulo * m_EscalaX); //Seta a velocidade
			}
		}
		else{//Caso o bot�o n�o esteja pressionado
			if (m_Modulo != 0){ //Verifica se h� movimento
				m_Aceleracao.SetarY(m_FatFre * -m_EscalaY); //Seta a acelera��o
				m_Aceleracao.SetarX(m_FatFre * m_EscalaX); //Seta a acelera��o
				m_Velocidade.SetarY(m_Modulo * -m_EscalaY); //Seta a velocidade
				m_Velocidade.SetarX(m_Modulo * m_EscalaX); //Seta a velocidade
				m_Modulo = m_Velocidade.Comprimento() - m_Aceleracao.Comprimento(); //Calcula o m�dulo com acelera��o negativa
				if (m_Modulo < 0.1){ //Evita movimento quando o m�dulo for muito pequeno
					m_Modulo = 0;
					m_bMovendo = false;
				}
			}
		}
		if (ManipuladorEntrada::Instancia()->TeclaPressionada(SDL_SCANCODE_S)){ //Se a seta para baixo for pressionada
			if (m_Modulo != 0){ //Verifica se h� movimento
				m_Aceleracao.SetarY(m_FatFre * -m_EscalaY); //Seta a acelera��o
				m_Aceleracao.SetarX(m_FatFre * m_EscalaX); //Seta a acelera��o
				m_Velocidade.SetarY(m_Modulo * -m_EscalaY); //Seta a velocidade
				m_Velocidade.SetarX(m_Modulo * m_EscalaX); //Seta a velocidade
				m_Modulo = m_Velocidade.Comprimento() - m_Aceleracao.Comprimento(); //Calcula o m�dulo com acelera��o negativa
				if (m_Modulo < 0.1){ //Evita movimento quando o m�dulo for muito pequeno
					m_Modulo = 0;
					m_bMovendo = false;
				}
			}
		}
	}
	else{
		if (m_Modulo != 0){ //Verifica se h� movimento
			m_Aceleracao.SetarY(m_FatFre * -m_EscalaY); //Seta a acelera��o
			m_Aceleracao.SetarX(m_FatFre * m_EscalaX); //Seta a acelera��o
			m_Velocidade.SetarY(m_Modulo * -m_EscalaY); //Seta a velocidade
			m_Velocidade.SetarX(m_Modulo * m_EscalaX); //Seta a velocidade
			m_Modulo = m_Velocidade.Comprimento() - m_Aceleracao.Comprimento(); //Calcula o m�dulo com acelera��o negativa
			if (m_Modulo < 0.1){ //Evita movimento quando o m�dulo for muito pequeno
				m_Modulo = 0;
				m_bMovendo = false;
			}
		}
	}
}