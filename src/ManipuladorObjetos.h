#ifndef __ManipuladorObjetos__
#define __ManipuladorObjetos__

#include <string>
#include <map>
#include "Objetos.h"

class CriadorBase{ //Classe que cria tipos de objetos
public:
	virtual Objetos * CriarObjeto() const = 0;
	virtual ~CriadorBase() {}
};

class ManipuladorObjetos{
public:
	//Instanciador para evitar que a classe seja criada mais de uma vez
	static ManipuladorObjetos * Instancia(){
		if (s_pInstancia == nullptr)
			s_pInstancia = new ManipuladorObjetos;
		return s_pInstancia;
	}

	bool RegistrarTipo(std::string TipoID, CriadorBase * pCriador){ //Registra o tipo de objeto
		std::map<std::string, CriadorBase*>::iterator it = m_Criador.find(TipoID);

		if (it != m_Criador.end()){ //Verifica se o tipo j� foi registrado
			delete pCriador;
			return false;
		}

		SDL_Log("Tipo criado com sucesso: %s", TipoID.c_str());
		m_Criador[TipoID] = pCriador;

		return true;
	}

	Objetos * Criar(std::string TipoID){ //Cria o objeto
		std::map<std::string, CriadorBase*>::iterator it = m_Criador.find(TipoID);

		if (it == m_Criador.end()){ //Verifica se existe o tipo de objeto que se deseja criar
			SDL_Log("Nao foi possivel achar o tipo: %s", TipoID.c_str());
			return 0;
		}

		CriadorBase * Criador = (*it).second;
		return Criador->CriarObjeto();
	}

private:
	ManipuladorObjetos() {}
	~ManipuladorObjetos() {}

	std::map<std::string, CriadorBase*> m_Criador;

	static ManipuladorObjetos * s_pInstancia;
};

#endif