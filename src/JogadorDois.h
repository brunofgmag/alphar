#ifndef __JogadorDois__
#define __JogadorDois__

#include "Jogadores.h"
#include "ManipuladorObjetos.h"

class JogadorDois : public Jogadores{
public:
	JogadorDois();
	~JogadorDois() {}

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();
	void Colisao();
	void Checkpoints(std::string ID);

private:
	void EntradaDados(); //Gerencia a entrada de dados do teclado
};

class CriadorJogadorDois : public CriadorBase{
	Jogadores * CriarObjeto() const{
		return new JogadorDois();
	}
};

#endif