#include "EstadoMenu.h"
#include "alphaR.h"


const std::string EstadoMenu::s_MenuID = "MENU";

void EstadoMenu::Atualizar(){
	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Atualizar();
		/*if (!m_bAlpha){
			if (m_pObjeto[i]->ObterTexturaID() == std::string("alphar")){
				if (SDL_GetTicks() - m_Iniciar > 500){
					if (m_Alpha <= 255){
						m_pObjeto[i]->SetarAlpha(m_Alpha);
						m_Alpha += 1;
					}
					else if(m_Alpha > 255){
						m_pObjeto[i]->SetarAlpha(255);
						m_bAlpha = true;
					}
				}
			}
		}*/
	}
}

void EstadoMenu::Desenhar(){
	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Desenhar();
	}
}

bool EstadoMenu::AoEntrar(){
	AnalisadorEstados analisadorEstados;
	analisadorEstados.AnalisarEstado("res/Estados.xml", s_MenuID, &m_pObjeto, &m_TexturasID);
	m_Callbacks.push_back(s_MenuParaJogo);
	m_Callbacks.push_back(s_SairDoMenu);

	//Seta os callbacks para os itens do menu
	SetarCallbacks(m_Callbacks);

	SDL_Log("Entrando no estado MENU");

	//m_bIniciado = true;

	return true;
}

void EstadoMenu::SetarCallbacks(const std::vector<Callback>& callbacks){
	if (!m_pObjeto.empty()){
		//Loop entre os objetos
		for (int i = 0; i < m_pObjeto.size(); i++){
			//Se eles forem do tipo BotaoMenu entao ser� designado um callback baseado no ID do arquivo XML
			if (dynamic_cast<BotaoMenu*>(m_pObjeto[i])){
				BotaoMenu * pBotao = dynamic_cast<BotaoMenu*>(m_pObjeto[i]);
				pBotao->SetarCallback(callbacks[pBotao->ObterCallbackID()]);
			}
		}
	}
}

bool EstadoMenu::AoSair(){
	for (int i = 0; i < m_pObjeto.size(); i++){
		m_pObjeto[i]->Limpar();
	}

	m_pObjeto.clear();
	SDL_Log("Saindo do estado MENU");
	return true;
}

void EstadoMenu::s_MenuParaJogo(){
	alphaR::Instancia()->ObterEstado()->MudarEstado(new EstadoJogo());
}

void EstadoMenu::s_SairDoMenu(){
	alphaR::Instancia()->Clean();
}