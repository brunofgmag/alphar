#ifndef __Parametros__
#define __Parametros__

#include <string>
#include "SDL.h"

class Parametros{ //Carregador de parametros
public:
	Parametros(std::string TexturaID, int X, int Y, int H, int W, int Frame = 0, int NumFrames = 0, int AnimSpeed = 0, int Coluna = 0, double Angulo = 0, SDL_Point * Ponto = 0, SDL_RendererFlip Flip = SDL_FLIP_NONE, int CallBackID = 0, int Alpha = 255) : m_TexturaID(TexturaID), m_X(X), m_Y(Y), m_H(H), m_W(W), m_Frame(Frame), m_NumFrames(NumFrames), m_AnimSpeed(AnimSpeed), m_Coluna(Coluna), m_Angulo(Angulo), m_pPonto(Ponto), m_Flip(Flip), m_CallBackID(CallBackID), m_Alpha(Alpha) {}

	std::string ObterTexturaID() const { return m_TexturaID; }
	int ObterX() const { return m_X; }
	int ObterY() const { return m_Y; }
	int ObterH() const { return m_H; }
	int ObterW() const { return m_W; }
	int ObterFrame() const { return m_Frame; }
	int ObterNumFrames() const { return m_NumFrames; }
	int ObterColuna() const { return m_Coluna; }
	int ObterAlpha() const { return m_Alpha; }
	double ObterAngulo() const { return m_Angulo; }
	SDL_Point * ObterPonto() const { return m_pPonto; }
	SDL_RendererFlip ObterFlip() const { return m_Flip; }
	int ObterCallBack() const { return m_CallBackID; }
	int ObterAnimSpeed() const { return m_AnimSpeed; }

private:
	std::string m_TexturaID;

	int m_X;
	int m_Y;
	int m_H;
	int m_W;
	int m_Frame;
	int m_NumFrames;
	int m_Coluna;
	int m_CallBackID;
	int m_AnimSpeed;
	double m_Angulo;
	SDL_Point * m_pPonto;
	SDL_RendererFlip m_Flip;
	int m_Alpha;
};

#endif;