#include "ManipuladorColisoes.h"
#include "alphaR.h"

void ManipuladorColisoes::ChecarJogadorTileColisao(Jogadores *pJogadores, const std::vector<CamadaTile*> ColisoesCamadas){
	for (std::vector<CamadaTile*>::const_iterator it = ColisoesCamadas.begin(); it != ColisoesCamadas.end(); ++it){
		CamadaTile* pCamadaTile = (*it);
		std::vector<std::vector<int>> Camadas = pCamadaTile->ObterTileIDs();

		//Obtem a posicao dessas camadas
		Vetor2D CamadaPos = pCamadaTile->ObterPosicao();

		int x, y, CamadaCol, CamadaLin, CamadaID = 0;

		//Calcula a posicao no mapa
		x = CamadaPos.ObterX() / pCamadaTile->ObterTileTamanho();
		y = CamadaPos.ObterY() / pCamadaTile->ObterTileTamanho();

		//Se movendo para direita e para baixo
		if (pJogadores->ObterVelocidade().ObterX() >= 0 || pJogadores->ObterVelocidade().ObterY() >= 0)
		{
			CamadaCol = ((pJogadores->ObterPosicao().ObterX() + pJogadores->ObterH()/1.3) / pCamadaTile->ObterTileTamanho());
			CamadaLin = ((pJogadores->ObterPosicao().ObterY() + pJogadores->ObterH()) / pCamadaTile->ObterTileTamanho());

			if (CamadaCol + x <= Camadas.size() && CamadaLin + y <= Camadas.size()){
				CamadaID = Camadas[CamadaLin + y][CamadaCol + x];
			}
		}
		//Se movendo para esquerda e para cima
		else if (pJogadores->ObterVelocidade().ObterX() < 0 || pJogadores->ObterVelocidade().ObterY() < 0)
		{
			CamadaCol = pJogadores->ObterPosicao().ObterX() / pCamadaTile->ObterTileTamanho();
			CamadaLin = pJogadores->ObterPosicao().ObterY() / pCamadaTile->ObterTileTamanho();

			if (CamadaLin + y <= Camadas.size() && CamadaCol + x <= Camadas.size()){
				CamadaID = Camadas[CamadaLin + y][CamadaCol + x];
			}
		}
		if (CamadaID != 0) // Se o CamadaID n�o for vazio ent�o colide
		{
			//Desativado - N�o colide com tile!
			//pJogadores->Colisao();
		}
	}
}

void ManipuladorColisoes::ChecarJogadorObjetoColisao(Jogadores *pJogadores, const std::vector<Objeto*> &objetos){
	SDL_Rect *pRect1 = new SDL_Rect();
	pRect1->x = pJogadores->ObterPosicao().ObterX();
	pRect1->y = pJogadores->ObterPosicao().ObterY();
	pRect1->h = pJogadores->ObterH();
	pRect1->w = pJogadores->ObterW();

	for (int i = 0; i != objetos.size(); i++){
		if (objetos[i]->Tipo() != "Objeto"){
			continue;
		}

		SDL_Rect *pRect2 = new SDL_Rect();
		pRect2->x = objetos[i]->ObterPosicao().ObterX();
		pRect2->y = objetos[i]->ObterPosicao().ObterY();
		pRect2->h = objetos[i]->ObterH();
		pRect2->w = objetos[i]->ObterW();

		if (m_Colisao.ColisaoObjetoCarro(pRect1, pRect2)){
			pJogadores->Colisao();
		}

		delete pRect2;
	}
	delete pRect1;
}

void ManipuladorColisoes::ChecarCheckpoint(Jogadores *pJogadores, const std::vector<Objeto*> &objetos){
	SDL_Rect *pRect1 = new SDL_Rect();
	pRect1->x = pJogadores->ObterPosicao().ObterX();
	pRect1->y = pJogadores->ObterPosicao().ObterY();
	pRect1->h = pJogadores->ObterH();
	pRect1->w = pJogadores->ObterW();

	for (int i = 0; i != objetos.size(); i++){
		if (objetos[i]->Tipo() != "Objeto"){
			continue;
		}

		SDL_Rect *pRect2 = new SDL_Rect();
		pRect2->x = objetos[i]->ObterPosicao().ObterX();
		pRect2->y = objetos[i]->ObterPosicao().ObterY();
		pRect2->h = objetos[i]->ObterH();
		pRect2->w = objetos[i]->ObterW();

		if (m_Colisao.ColisaoObjetoCarro(pRect1, pRect2)){
			pJogadores->Checkpoints(objetos[i]->ObterTexturaID());
		}

		delete pRect2;
	}
	delete pRect1;
}