#ifndef __GraficoAnimado__
#define __GraficoAnimado__

#include "Objeto.h"
#include "ManipuladorObjetos.h"

class GraficoAnimado : public Objeto{
public:
	GraficoAnimado();

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();
};

class CriadorGraficoAnimado : public CriadorBase{
	Objeto * CriarObjeto() const{
		return new GraficoAnimado();
	}
};


#endif