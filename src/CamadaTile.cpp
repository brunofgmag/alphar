#include "CamadaTile.h"
#include "alphaR.h"
#include <iostream>

CamadaTile::CamadaTile(int TamanhoTile, const std::vector<Tileset> &Tilesets) : m_TileTamanho(TamanhoTile), m_Tilesets(Tilesets), m_Posicao(0, 0), m_Velocidade(0, 0)
{
	//Obt�m somente os valores desejados para desenhar (Somente os que est�o aparecendo na tela)
	m_NumColunas = (alphaR::Instancia()->ObterH() / m_TileTamanho);
	m_NumLinhas = (alphaR::Instancia()->ObterW() / m_TileTamanho);
}

void CamadaTile::Desenhar(){
	int x, y, x2, y2 = 0;

	x = m_Posicao.ObterX() / m_TileTamanho;
	y = m_Posicao.ObterY() / m_TileTamanho;

	x2 = int(m_Posicao.ObterX()) % m_TileTamanho;
	y2 = int(m_Posicao.ObterY()) % m_TileTamanho;

	/*
	std::cout << x << std::endl;
	std::cout << x2 << std::endl;
	std::cout << y << std::endl;
	std::cout << y2 << std::endl;
	*/

	//Loop entre as linhas e colunas
	for (int i = 0; i < m_NumLinhas; i++){
		for (int j = 0; j < m_NumColunas; j++){
			//Obt�m a ID atual do tile
			int id = m_TileIDs[i + y][j + x];
			//Checa se o ID � 0, se for, pula para o pr�ximo loop (comando continue). (n�o desenha)
			if (id == 0){
				continue;
			}
			//Caso contr�rio pega o tileset correto
			Tileset tileset = ObterTilesetPorID(id);

			id--;

			//Desenha os tiles
			/*
			std::cout << m_NumColunas << std::endl;
			std::cout << tileset.NumColunas << std::endl;
			std::cout << m_TileTamanho << std::endl;
			std::cout << tileset.PrimeiroGridID << std::endl;
			std::cout << tileset.Margem << std::endl;
			*/
			m_TileNome = tileset.Nome;

			Textura::Instancia()->DesenharTile(tileset.Nome, tileset.Margem, tileset.Espacamento, (j * m_TileTamanho) - x2, (i * m_TileTamanho) - y2, m_TileTamanho, m_TileTamanho, (id - (tileset.PrimeiroGridID - 1)) / tileset.NumColunas, (id - (tileset.PrimeiroGridID - 1)) % tileset.NumColunas, alphaR::Instancia()->GetRenderer());
		}
	}
}

Tileset CamadaTile::ObterTilesetPorID(int TileID){ //Compara cada valor de tileset "firstguid" e retorna o tileset correto
	for (int i = 0; i < m_Tilesets.size(); i++){
		if (i + 1 <= m_Tilesets.size() - 1){
			if (TileID >= m_Tilesets[i].PrimeiroGridID && TileID < m_Tilesets[i + 1].PrimeiroGridID){
				return m_Tilesets[i];
			}
		}
		else{
			return m_Tilesets[i];
		}
	}

	SDL_Log("Nao foi possivel achar o tileset, retornando um tileset vazio");
	Tileset t;
	return t;
}

void CamadaTile::Atualizar(Pista *pPista){
	m_Posicao += m_Velocidade;
}

void CamadaTile::Limpar(){
	Textura::Instancia()->Apagar("tilepista");
}