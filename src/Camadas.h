#ifndef __Camadas__
#define __Camadas__

class Pista;

class Camadas{ //Classe abstrata camadas
public:
	virtual void Desenhar() = 0;
	virtual void Atualizar(Pista *pPista) = 0;
	virtual void Limpar() = 0;

protected:
	virtual ~Camadas() {}
};

#endif