#ifndef __Carros__
#define __Carros__

#include "Objetos.h"

class Carros : public Objetos{
public:
	~Carros() {}

	void Carregar(std::unique_ptr<Parametros> const &P);
	void Desenhar();
	void Atualizar();
	void Limpar();
	void Colisao() {}
	std::string Tipo() { return "Carros"; }

protected:
	Carros();

	double m_AnguloTemp = m_Angulo; //Define variável temporária para manipulação de angulo
	double m_Modulo = 0;
	float m_FatAcel = 0.10; //Fator de aceleração
	float m_FatFre = 0.10; //Fator de frenagem
};

#endif