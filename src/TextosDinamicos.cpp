#include "TextosDinamicos.h"
#include "Textura.h"
#include "alphaR.h"

TextosDinamicos::TextosDinamicos() : Objeto(){
}

void TextosDinamicos::Carregar(std::unique_ptr<Parametros> const &P, std::string Texto, int Tamanho, std::string Fonte, int R, int G, int B){
	//Preenche os membros com os valores declarados no par�metro
	m_Posicao = Vetor2D(P->ObterX(), P->ObterY());
	m_Velocidade = Vetor2D(0, 0);
	m_Aceleracao = Vetor2D(0, 0);
	m_W = P->ObterW();
	m_H = P->ObterH();
	m_Frame = P->ObterFrame();
	m_AnimSpeed = P->ObterAnimSpeed();
	m_NumFrames = P->ObterNumFrames();
	m_Coluna = P->ObterColuna();
	m_TexturaID = P->ObterTexturaID();
	m_Angulo = P->ObterAngulo();
	m_pPonto = P->ObterPonto();
	m_Flip = P->ObterFlip();
	m_Alpha = P->ObterAlpha();
	m_Texto = Texto;
	m_Fonte = Fonte;
	m_Tamanho = Tamanho;
	m_R = R;
	m_G = G;
	m_B = B;
}

void TextosDinamicos::Desenhar(){
	Objeto::Desenhar();
}

void TextosDinamicos::Atualizar(){
	Objeto::Atualizar();
}

void TextosDinamicos::Limpar(){
	Objeto::Limpar();
}

void TextosDinamicos::SetarTexto(std::string ID, std::string Texto){
	if (m_TexturaID == std::string(ID)){
		this->Limpar();
		SDL_Color Cor = { m_R, m_G, m_B };
		Textura::Instancia()->CarregarTexto(m_Fonte, ID, alphaR::Instancia()->GetRenderer(), m_Tamanho, Texto, Cor);
	}
}