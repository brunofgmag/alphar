#include "ImagemEstatica.h"
#include "Textura.h"
#include "alphaR.h"

ImagemEstatica::ImagemEstatica() : Objeto(){
}

void ImagemEstatica::Carregar(std::unique_ptr<Parametros> const &P){
	Objeto::Carregar(P);
}

void ImagemEstatica::Desenhar(){
	Objeto::Desenhar();
}

void ImagemEstatica::Atualizar(){
	Objeto::Atualizar();
}

void ImagemEstatica::Limpar(){
	Objeto::Limpar();
}