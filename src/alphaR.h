#ifndef __alphaR__
#define __alphaR__

#include "SDL.h" //Carrega biblioteca do SDL
#include "SDL_image.h" //Carrega biblioteca dos formatos de imagem
#include "SDL_ttf.h" //Carrega biblioteca de fontes
#include "Textura.h" //Carrega biblioteca das texturas do jogo
#include <vector> //Inclui biblioteca de vetores do C++
#include "Carros.h" //Inclui biblioteca de objetos do jogo
#include "ManipuladorEntrada.h" //Inclui a biblioteca de manipula��o de entrada de dados
#include "JogadorUm.h"
#include "JogadorDois.h"
//#include "CarroAzul.h"
//#include "Colisoes.h" //Inclui a biblioteca de colis�es
#include "ManipuladorObjetos.h" //Inclui biblioteca de cria��o e manipula��o de objetos
#include "ManipuladorEstados.h" //Inclui biblioteca de manipula��o de estados
#include "EstadoMenu.h" //Biblioteca do estado menu
#include "EstadoJogo.h" //Biblioteca do estado jogo
#include "EstadoPausa.h" //Biblioteca do estado pausa
#include "BotaoMenu.h" //Biblioteca dos botoes do menu
#include "GraficoAnimado.h" //Biblioteca para anima��o de graficos
#include "TextosDinamicos.h" //Biblioteca para alterar textos
#include "ImagemEstatica.h" //Biblioteca para desenho de imagens simples
#include "AnalisadorEstados.h" //Blibloteca de an�lise de estados atrav�s de arquivos XML
#include "AnalisadorPista.h" //Biblioteca de ana�lise de pistas atr�ves de um arquivo TXM

class alphaR
{
public:
	~alphaR() {}

	//Instanciador para evitar que a classe seja criada mais de uma vez
	static alphaR * Instancia(){
		if (s_pInstancia == nullptr)
			s_pInstancia = new alphaR;
		return s_pInstancia;
	}

	//M�todos do jogo
	bool init(const char * Titulo, int PosX, int PosY, int Altura, int Largura, int Flags);
	void Render();
	void Update();
	void HandleEvents();
	void Clean();

	//Acessando membros privados
	bool Rodando(){ return m_Rodando; }
	SDL_Renderer * GetRenderer() const { return m_pRenderer; } //Retorna o renderer
	ManipuladorEstados * ObterEstado(){ return m_Estados; }
	int ObterW() const { return m_Largura; }
	int ObterH() const { return m_Altura; }

private:
	alphaR() {}

	static alphaR * s_pInstancia; //Declara o ponteiro do objeto est�tico Instancia

	int m_Altura = 960;
	int m_Largura = 704;

	SDL_Window * m_pJanela;
	SDL_Renderer * m_pRenderer;
	ManipuladorEstados * m_Estados;

	bool m_Rodando;
};

#endif