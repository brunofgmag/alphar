#include "CarroAzul.h"
#include "alphaR.h"

CarroAzul::CarroAzul() : Objeto(){
}

void CarroAzul::Carregar(const Parametros * P){
	//Preenche os membros com os valores declarados no par�metro
	Objeto::Carregar(P);
}

void CarroAzul::Desenhar(){
	if (m_Velocidade.ObterX() < 0)
		m_Angulo = 90.0;
	else
		m_Angulo = -90.0;

	Objeto::Desenhar();
}

void CarroAzul::Atualizar(){
	if (m_Posicao.ObterX() < 0)
	{
		m_Velocidade.SetarX(m_MaxVeloc);
	}

	if (m_Posicao.ObterX() > 400)
	{
		m_Velocidade.SetarX(-m_MaxVeloc);
	}

	m_Posicao += m_Velocidade;

	Objeto::Atualizar();
}

void CarroAzul::Limpar(){
	Objeto::Limpar();
}