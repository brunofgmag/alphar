#ifndef __CamadaTile__
#define __CamadaTile__

#include "Camadas.h"
#include "Pista.h"
#include "Vetor2D.h"

class CamadaTile : public Camadas{ //Classe que armazena a informa��o do tile da camada
public:
	CamadaTile(int TamanhoTile, const std::vector<Tileset> &Tilesets);

	void Desenhar();
	void Atualizar(Pista *pPista);
	void Limpar();

	void SetarTileIDs(const std::vector<std::vector<int>> &data){ //Define a ID do tile
		m_TileIDs = data;
	}

	void SetarTileTamanho(int TileTamanho){ //Define o tamanho do tile
		m_TileTamanho = TileTamanho;
	}

	int ObterTileTamanho(){ return m_TileTamanho; }

	const std::vector<std::vector<int>> &ObterTileIDs() { return m_TileIDs; }
	const Vetor2D ObterPosicao() { return m_Posicao; }

	Tileset ObterTilesetPorID(int TileID); //Obt�m o tile atrav�s de um ID previamente setado

private:
	int m_NumColunas;
	int m_NumLinhas;
	int m_TileTamanho;
	std::string m_TileNome;

	Vetor2D m_Posicao;
	Vetor2D m_Velocidade;

	const std::vector<Tileset> &m_Tilesets;
	std::vector<std::vector<int>> m_TileIDs;
};

#endif