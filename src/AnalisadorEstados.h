#ifndef __AnalisadorEstados__
#define __AnalisadorEstados__

#include <vector>
#include "Objetos.h"
#include "tinyxml.h"
#include "SDL.h"

class AnalisadorEstados{
public:
	AnalisadorEstados(){}
	~AnalisadorEstados(){}

	bool AnalisarEstado(const char *ArquivoEstado, std::string EstadoID, std::vector<Objetos*> *pObjetos, std::vector<std::string> *TexturasIDs);

private:
	void AnalisarObjetos(TiXmlElement *pEstadoRaiz, std::vector<Objetos*> *pObjetos);
	void AnalisarTexturas(TiXmlElement *pEstadoRaiz, std::vector<std::string> *TexturasIDs);
	void CarregarTexto(TiXmlElement *pEstadoRaiz, std::vector<Objetos*> *pObjetos, std::vector<std::string> *TexturasIDs);
	void CarregarTextoDinamico(TiXmlElement *pEstadoRaiz, std::vector<Objetos*> *pObjetos, std::vector<std::string> *TexturasIDs);
	SDL_RendererFlip m_Flip;
};

#endif