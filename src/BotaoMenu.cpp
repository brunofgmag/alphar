#include "BotaoMenu.h"
#include "ManipuladorEntrada.h"

BotaoMenu::BotaoMenu() : Carros(), m_Callback(0){
}

void BotaoMenu::Carregar(std::unique_ptr<Parametros> const &P){
	Carros::Carregar(P);
	m_CallbackID = P->ObterCallBack();
	m_Frame = MOUSE_FORA;
}

void BotaoMenu::Desenhar(){
	Carros::Desenhar();
}

void BotaoMenu::Atualizar(){
	Vetor2D PosicaoMouse = ManipuladorEntrada::Instancia()->PosicaoMouse(); //Obtem posicao do mouse e salva em um vetor

	//Verifica se o mouse esta dentro da area do botao
	if (PosicaoMouse.ObterX() < (m_Posicao.ObterX() + m_W) && PosicaoMouse.ObterX() > m_Posicao.ObterX() && PosicaoMouse.ObterY() < (m_Posicao.ObterY() + m_H) && PosicaoMouse.ObterY() > (m_Posicao.ObterY())){
		m_Frame = MOUSE_DENTRO; //Caso sim muda o frame da imagem

		if (ManipuladorEntrada::Instancia()->ObterBotaoMouse(LEFT) && m_bSolto){ //Caso o usuario clique com o botao esquerdo (e segure), o frame da imagem muda novamente
			m_Frame = CLICK;
			m_Callback(); //Chama a func�o Callback
			m_bSolto = false;
		}
		else if (!ManipuladorEntrada::Instancia()->ObterBotaoMouse(LEFT)){ //Caso o usuario solte o botao do mouse
			m_bSolto = true;
			m_Frame = MOUSE_DENTRO;
		}
	}
	else{ //Caso contrario volta ao frame 0
		m_Frame = MOUSE_FORA;
	}
}

void BotaoMenu::Limpar(){
	Carros::Limpar();
}