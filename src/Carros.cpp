#include "Carros.h"
#include "Textura.h"
#include "alphaR.h"

Carros::Carros() : Objetos()  {
}

void Carros::Carregar(std::unique_ptr<Parametros> const &P){
	//Preenche os membros com os valores declarados no par�metro
	m_Posicao = Vetor2D(P->ObterX(), P->ObterY());
	m_Velocidade = Vetor2D(0, 0);
	m_Aceleracao = Vetor2D(0, 0);
	m_W = P->ObterW();
	m_H = P->ObterH();
	m_Frame = P->ObterFrame();
	m_AnimSpeed = P->ObterAnimSpeed();
	m_NumFrames = P->ObterNumFrames();
	m_Coluna = P->ObterColuna();
	m_TexturaID = P->ObterTexturaID();
	m_Angulo = P->ObterAngulo();
	m_pPonto = P->ObterPonto();
	m_Flip = P->ObterFlip();
	m_Alpha = P->ObterAlpha();
}

void Carros::Desenhar(){
	Textura::Instancia()->DesenharFrame(m_TexturaID, alphaR::Instancia()->GetRenderer(), (Uint32)m_Posicao.ObterX(), (Uint32)m_Posicao.ObterY(), m_H, m_W, m_Frame, m_Coluna, m_Angulo, m_Alpha, m_pPonto, m_Flip);
	
	/*if (m_Velocidade.ObterY() > 0)
		Textura::Instancia()->DesenharFrame(m_TexturaID, alphaR::Instancia()->GetRenderer(), (Uint32)m_Posicao.ObterX(), (Uint32)m_Posicao.ObterY(), m_H, m_W, m_Frame, m_Coluna, m_Angulo, m_pPonto, SDL_FLIP_NONE);
	else
		Textura::Instancia()->DesenharFrame(m_TexturaID, alphaR::Instancia()->GetRenderer(), (Uint32)m_Posicao.ObterX(), (Uint32)m_Posicao.ObterY(), m_H, m_W, m_Frame, m_Coluna, m_Angulo, m_pPonto, SDL_FLIP_VERTICAL);
	*/
}

void Carros::Atualizar(){
	//m_Velocidade += m_Aceleracao;
}

void Carros::Limpar(){
	Textura::Instancia()->Apagar(m_TexturaID);
}
